﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Zadanie2.Scyscrapper
{
    public class ScyScrapper
    {
        public ScyscrapperItem[,] scyscrapper { get; set; }
        public static List<ScyScrapper> CorrectResults = new List<ScyScrapper>();
        public static int wglebien = 0;
        public static int przypisan = 0;

        public static int przypierwszejwglebien = 0;
        public static int przypierwszejprzypisan = 0;
        public static long czasdopierwszego = 0;
        public int Size { get; set; }


        public static Stopwatch stopwatch = new Stopwatch();

        public ScyScrapper(int size)
        {
            this.Size = size;
            scyscrapper = new ScyscrapperItem[size + 2, size + 2];
            for (int a = 1; a <= Size; ++a)
                for (int b = 1; b <= Size; ++b)
                    scyscrapper[a, b] = new ScyscrapperItem();
        }

        public ScyScrapper(ScyScrapper copyFrom)
        {
            Size = copyFrom.Size;
            scyscrapper = new ScyscrapperItem[Size + 2, Size + 2];
            for (int a = 0; a < Size + 2; ++a)
                for (int b = 0; b < Size + 2; ++b)
                    scyscrapper[a, b] = new ScyscrapperItem(copyFrom.scyscrapper[a, b]);
        }

        
        private bool Correct(int row, int column)
        {
            
            //wartosci
            List<int> rows = new List<int>();
            List<int> columns = new List<int>();
            for (int j = 1; j <= Size; ++j)
            {
                if (scyscrapper[row,j].Value != 0)
                {
                    if (rows.Contains(scyscrapper[row, j].Value))
                        return false;
                    rows.Add(scyscrapper[row, j].Value);
                }

                if (scyscrapper[j, column].Value != 0)
                {
                    if (columns.Contains(scyscrapper[j, column].Value))
                        return false;
                    columns.Add(scyscrapper[j, column].Value);
                }
                
            }


            if(rows.Count==Size)
            {
                //od lewej
                
                    int correctlyTimes = scyscrapper[row, 0].Value;
                    if (correctlyTimes != 0)
                    {
                        int times = 1;
                        int biggest = scyscrapper[row, 1].Value;
                        for (int j = 1; j <= Size; ++j)
                        {
                            if (biggest < scyscrapper[row, j + 1].Value)
                            {
                                ++times;
                                biggest = scyscrapper[row, j + 1].Value;
                            }
                        }
                        if (times != correctlyTimes)
                            return false;
                    }
                

                //od prawej
                    correctlyTimes = scyscrapper[row, Size + 1].Value;
                    if (correctlyTimes != 0)
                    {
                        int times = 1;
                        int biggest = scyscrapper[row, Size].Value;
                        for (int j = Size; j > 0; --j)
                        {
                            if (biggest < scyscrapper[row, j - 1].Value)
                            {
                                ++times;
                                biggest = scyscrapper[row, j - 1].Value;
                            }
                        }
                        if (times != correctlyTimes)
                            return false;
                    }
                
            }

            if(columns.Count==Size)
            {
                //od góry
                
                    int correctlyTimes = scyscrapper[0, column].Value;
                    if (correctlyTimes != 0)
                    {
                        int times = 1;
                        int biggest = scyscrapper[1,column].Value;
                        for (int j = 1; j <= Size; ++j)
                        {
                            if (biggest < scyscrapper[j + 1, column].Value)
                            {
                                ++times;
                                biggest = scyscrapper[j + 1, column].Value;
                            }
                        }
                        if (times != correctlyTimes)
                            return false;
                    }
                

                //od dołu
                    correctlyTimes = scyscrapper[Size + 1, column].Value;
                    if (correctlyTimes != 0)
                    {
                        int times = 1;
                        int biggest = scyscrapper[Size, column].Value;
                        for (int j = Size; j > 0; --j)
                        {
                            if (biggest < scyscrapper[j - 1, column].Value)
                            {
                                ++times;
                                biggest = scyscrapper[j - 1, column].Value;
                            }
                        }
                        if (times != correctlyTimes)
                            return false;
                    }
                
            }



            return true;
        }

        public void FillPossibleValues()
        {
            for (int i = 1; i <= Size; ++i)
                for (int j = 1; j <= Size; ++j)
                {
                    if (scyscrapper[i, j].Value == 0)
                    {
                        scyscrapper[i, j].PossiblyValues.Clear();
                        for (int times = 1; times <= Size; ++times)
                            scyscrapper[i, j].PossiblyValues.Add(times);
                    }
                }
            for (int i = 1; i <= Size; ++i)
                for (int j = 1; j <= Size; ++j)
                {
                    if (scyscrapper[i, j].Value != 0)
                    {
                        RemovePossibleValuesForItem(i, j, scyscrapper[i, j].Value);
                    }
                }
        }

        public bool Correct()
        {
            //od lewej
            for (int i = 1; i <= Size; ++i)
            {
                int correctlyTimes = scyscrapper[i, 0].Value;
                if (correctlyTimes != 0)
                {
                    int times = 1;
                    int biggest = scyscrapper[i, 1].Value;
                    for (int j = 1; j <= Size; ++j)
                    {
                        if (biggest < scyscrapper[i, j + 1].Value)
                        {
                            ++times;
                            biggest = scyscrapper[i, j + 1].Value;
                        }
                    }
                    if (times != correctlyTimes)
                        return false;
                }
            }

            //od prawej
            for (int i = 1; i <= Size; ++i)
            {
                int correctlyTimes = scyscrapper[i, Size + 1].Value;
                if (correctlyTimes != 0)
                {
                    int times = 1;
                    int biggest = scyscrapper[i, Size].Value;
                    for (int j = Size; j > 0; --j)
                    {
                        if (biggest < scyscrapper[i, j - 1].Value)
                        {
                            ++times;
                            biggest = scyscrapper[i, j - 1].Value;
                        }
                    }
                    if (times != correctlyTimes)
                        return false;
                }
            }

            //od góry
            for (int i = 1; i <= Size; ++i)
            {
                int correctlyTimes = scyscrapper[0, i].Value;
                if (correctlyTimes != 0)
                {
                    int times = 1;
                    int biggest = scyscrapper[1, i].Value;
                    for (int j = 1; j <= Size; ++j)
                    {
                        if (biggest < scyscrapper[j + 1, i].Value)
                        {
                            ++times;
                            biggest = scyscrapper[j + 1, i].Value;
                        }
                    }
                    if (times != correctlyTimes)
                        return false;
                }
            }

            //od dołu
            for (int i = 1; i <= Size; ++i)
            {
                int correctlyTimes = scyscrapper[Size + 1, i].Value;
                if (correctlyTimes != 0)
                {
                    int times = 1;
                    int biggest = scyscrapper[Size, i].Value;
                    for (int j = Size; j > 0; --j)
                    {
                        if (biggest < scyscrapper[j - 1, i].Value)
                        {
                            ++times;
                            biggest = scyscrapper[j - 1, i].Value;
                        }
                    }
                    if (times != correctlyTimes)
                        return false;
                }
            }
            //wartosci
            for (int i = 1; i <= Size; ++i)
            {
                List<int> rows = new List<int>();
                List<int> columns = new List<int>();
                for (int j = 1; j <= Size; ++j)
                {
                    if (scyscrapper[i, j].Value == 0 || scyscrapper[j, i].Value == 0)
                        return false;
                    rows.Add(scyscrapper[i, j].Value);
                    columns.Add(scyscrapper[j, i].Value);
                }
                if ((rows.Distinct().Count() != Size) || (columns.Distinct().Count() != Size))
                    return false;
            }

            return true;
        }


        public void BackTracking(ScyScrapper s)
        {
            for (int i = 1; i <= Size; ++i)
            {
                for (int j = 1; j <= Size; ++j)
                {
                    if (s.scyscrapper[i, j].Value == 0)
                    {
                        for (int p = 0; p < s.scyscrapper[i, j].PossiblyValues.Count; ++p)
                        {
                            ScyScrapper next = new ScyScrapper(s);
                            next.scyscrapper[i, j].Value = next.scyscrapper[i, j].PossiblyValues[p];
                            ++przypisan;
                            if (next.Correct(i, j))
                            {
                                ++wglebien;
                                BackTracking(next);
                            }
                            if (next.Correct())
                            {
                                stopwatch.Stop();
                                czasdopierwszego = stopwatch.ElapsedMilliseconds;
                                stopwatch.Start();
                                CorrectResults.Add(new ScyScrapper(next));
                                if (przypierwszejprzypisan == 0)
                                {
                                    przypierwszejprzypisan = przypisan;
                                    przypierwszejwglebien = wglebien;
                                }
                            }
                            else
                                if (p >= s.scyscrapper[i, j].PossiblyValues.Count - 1)
                                return;
                        }
                    }
                }
            }
        }

        public void ForwardChecking(ScyScrapper s)
        {
            for (int i = 1; i <= Size; ++i)
            {
                for (int j = 1; j <= Size; ++j)
                {
                    if (s.scyscrapper[i, j].Value == 0)
                    {
                        for (int p = 0; p < s.scyscrapper[i, j].PossiblyValues.Count; ++p)
                        {
                            ScyScrapper next = new ScyScrapper(s);
                            next.scyscrapper[i, j].Value = s.scyscrapper[i, j].PossiblyValues[p];
                            ++przypisan;
                            next.RemovePossibleValuesForItem(i, j, next.scyscrapper[i, j].Value);
                            if (next.Correct(i,j) && next.ContainsMinimumOne())
                            {
                                ++wglebien;
                                ForwardChecking(next);
                            }
                            if (next.Correct())
                            {
                                stopwatch.Stop();
                                czasdopierwszego = stopwatch.ElapsedMilliseconds;
                                stopwatch.Start();
                                CorrectResults.Add(new ScyScrapper(next));
                                if (przypierwszejprzypisan == 0)
                                {
                                    przypierwszejprzypisan = przypisan;
                                    przypierwszejwglebien = wglebien;
                                }
                            }
                            else
                                if (p >= s.scyscrapper[i, j].PossiblyValues.Count - 1)
                                return;
                        }
                    }
                }
            }
        }


        private bool ContainsMinimumOne()
        {
            for (int i = 1; i <= Size; ++i)
            {
                for (int j = 1; j <= Size; ++j)
                {
                    if (scyscrapper[i, j].Value == 0 && scyscrapper[i, j].PossiblyValues.Count == 0)
                        return false;
                }
            }
            return true;
        }

        private void RemovePossibleValuesForItem(int row, int column, int value)
        {
            for (int i = 1; i <= Size; ++i)
            {
                if(scyscrapper[i, column].Value==0)
                scyscrapper[i, column].PossiblyValues.Remove(value);
            }

            for (int i = 1; i <= Size; ++i)
            {
                if (scyscrapper[row, i].Value == 0)
                    scyscrapper[row, i].PossiblyValues.Remove(value);
            }
        }

        public override string ToString()
        {
            string s = "";
            for (int i = 0; i < Size + 2; ++i)
            {
                for (int j = 0; j < Size + 2; ++j)
                {
                    if (scyscrapper[i, j] != null)
                        s += scyscrapper[i, j].Value + " ";
                    else
                        s += "b";
                }
                s += "\n";
            }
            Console.WriteLine(s);
            return s;
        }
    }
}
