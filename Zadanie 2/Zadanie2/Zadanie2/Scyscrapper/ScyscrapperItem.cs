﻿using System.Collections.Generic;

namespace Zadanie2.Scyscrapper
{
    public class ScyscrapperItem : Item
    {
        public ScyscrapperItem()
        {
        }

        public ScyscrapperItem(Item copyFrom) : base(copyFrom)
        {

        }
        public ScyscrapperItem(int value) : base()
        {
            Value = value;
        }
    }
}
