﻿using System;
using System.Diagnostics;
using System.IO;
using Zadanie2.Scyscrapper;

namespace Zadanie2
{
    public class Program
    {
        public static void save()
        {
            using (StreamWriter outputFile = File.AppendText("Results.txt"))
            {
                outputFile.WriteLine(Futoshiki.CorrectResults[0].ToString());
            }
        }

        public static void saveScy()
        {
            using (StreamWriter outputFile = new StreamWriter("Scyscrapper.txt"))
            {
                foreach(var r in ScyScrapper.CorrectResults)
                outputFile.WriteLine(r.ToString());
            }
        }
        static void Main(string[] args)
        {
            //ReadData readData = new ReadData(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\artificial-intelligence\Zadanie 2\CSP_2019_dane_badawcze_v1.2\test_futo_5_2.txt");
            //Futoshiki futoshiki = readData.ReadFutoshikiFile();
            //futoshiki.FillPossibleValues();




            //Futoshiki.stopwatch.Start();
            //futoshiki.BackTrackingWithVariables(futoshiki);
            ////futoshiki.ForwardCheckingWithVariables(futoshiki);
            //Futoshiki.stopwatch.Stop();

            //Console.WriteLine("czas: " + Futoshiki.stopwatch.ElapsedMilliseconds);
            //Console.WriteLine("czas do pierwszego: " + Futoshiki.czasdopierwszego);
            //Console.WriteLine("przypisan : " + Futoshiki.przypisan);
            //Console.WriteLine("przypisan do pierwszego: " + Futoshiki.przypierwszejprzypisan);
            //Console.WriteLine("wglebien : " + Futoshiki.wglebien);
            //Console.WriteLine("wglebien do pierwszego: " + Futoshiki.przypierwszejwglebien);
            //Futoshiki.ClearData();








            ReadData readDataScyscrapper = new ReadData(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\artificial-intelligence\Zadanie 2\CSP_2019_dane_badawcze_v1.2\test_sky_4_3.txt");
            ScyScrapper scyscrapper = readDataScyscrapper.ReadSkyscrapperFile();
            scyscrapper.FillPossibleValues();


            ScyScrapper.stopwatch.Start();
            //scyscrapper.BackTracking(scyscrapper);
            scyscrapper.ForwardChecking(scyscrapper);
            ScyScrapper.stopwatch.Stop();


            Console.WriteLine("czas: " + ScyScrapper.stopwatch.ElapsedMilliseconds);
            Console.WriteLine("czas do pierwszego: " + ScyScrapper.czasdopierwszego);
            Console.WriteLine("przypisan : " + ScyScrapper.przypisan);
            Console.WriteLine("przypisan do pierwszego: " + ScyScrapper.przypierwszejprzypisan);
            Console.WriteLine("wglebien : " + ScyScrapper.wglebien);
            Console.WriteLine("wglebien do pierwszego: " + ScyScrapper.przypierwszejwglebien);

            var a = 5;

















            //using (StreamWriter outputFile = new StreamWriter("Results.txt"))
            //{
            //    outputFile.WriteLine("nazwa; rozwiazan;przypisanDoPierwszego; wglebienDoPierwszego; przypisan; wglebien;czas");
            //    stopwatch.Start();
            //    futoshiki.ForwardCheckingNormal(futoshiki);
            //    stopwatch.Stop();  
            //    outputFile.WriteLine($"ForwardChecking;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();

            //    stopwatch.Start();
            //    futoshiki.ForwardCheckingWithBestValue(futoshiki);
            //    stopwatch.Stop();
            //    outputFile.WriteLine($"ForwardCheckingWithBestValue;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();



            //    stopwatch.Start();
            //    futoshiki.ForwardCheckingWithVariables(futoshiki);
            //    stopwatch.Stop();
            //    outputFile.WriteLine($"ForwardCheckingWithVariables;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();



            //    stopwatch.Start();
            //    futoshiki.ForwardCheckingWithVariablesAndValues(futoshiki);
            //    stopwatch.Stop();
            //    outputFile.WriteLine($"ForwardCheckingWithVariablesAndValues;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();



            //    stopwatch.Start();
            //    futoshiki.BackTracking(futoshiki);
            //    stopwatch.Stop();
            //    outputFile.WriteLine($"BackTracking;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();



            //    stopwatch.Start();
            //    futoshiki.BackTrackingWithBestValue(futoshiki);
            //    stopwatch.Stop();
            //    outputFile.WriteLine($"BackTrackingWithBestValue;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();



            //    stopwatch.Start();
            //    futoshiki.BackTrackingWithVariablesAndValues(futoshiki);
            //    stopwatch.Stop();
            //    outputFile.WriteLine($"BackTrackingWithVariablesAndValues;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();



            //    stopwatch.Start();
            //    futoshiki.BackTrackingWithVariables(futoshiki);
            //    stopwatch.Stop();
            //    outputFile.WriteLine($"BackTrackingWithVariables;{Futoshiki.CorrectResults.Count};{Futoshiki.przypierwszejprzypisan};{Futoshiki.przypierwszejwglebien};{Futoshiki.przypisan};{Futoshiki.wglebien};{stopwatch.ElapsedMilliseconds}");
            //    Futoshiki.ClearData();
            //    stopwatch.Reset();


            //}



            //ReadData readDataScyscrapper = new ReadData(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\artificial-intelligence\Zadanie 2\CSP_2019_dane_badawcze_v1.2\test_sky_5_2.txt");
            //ScyScrapper scyscrapper = readDataScyscrapper.ReadSkyscrapperFile();
            //scyscrapper.FillPossibleValues();
            ////scyscrapper.BackTracking(scyscrapper);
            //scyscrapper.ForwardChecking(scyscrapper);
            //saveScy();
            //int res = ScyScrapper.CorrectResults.Count;
            //int res2 = ScyScrapper.wglebien;

        }
    }
}
