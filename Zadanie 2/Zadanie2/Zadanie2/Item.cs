﻿using System;
using System.Collections.Generic;

namespace Zadanie2
{
    public abstract class Item
    {
        public int Value { get; set; }

        public List<int> PossiblyValues { get; set; } = new List<int>();

        public Item()
        {

        }

        public Item(Item copyFrom)
        {
            if (copyFrom != null)
            {
                Value = copyFrom.Value;
                foreach (var i in copyFrom.PossiblyValues)
                    PossiblyValues.Add(i);
            }
        }

        public override string ToString()
        {
            string v = "";
            foreach (var a in PossiblyValues)
            {
                v += a + " ";
            }
            string s = $"Value {Value} możliwe: {v}";
            Console.WriteLine(s);
            return s;
        }
    }
}
