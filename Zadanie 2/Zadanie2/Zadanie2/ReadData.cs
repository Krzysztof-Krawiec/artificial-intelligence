﻿using System.IO;
using Zadanie2.Scyscrapper;
using System.Linq;

namespace Zadanie2
{
    public class ReadData
    {
        const int FutoshikiFirstLines = 3;
        const int ScyscrapperFirstLines = 1;
        public ReadData(string path)
        {
            Path = path;
        }

        public string Path { get; set; }
        public Futoshiki ReadFutoshikiFile()
        {
            Futoshiki fustoshiki =null;
            var lines = File.ReadLines(Path);
            int length = lines.Count();
            int i = 1;
            int row = 0;
            int size = 0;
            foreach(var line in lines)
            {
                switch (i)
                {
                    case 1:
                        size = int.Parse(line);
                        fustoshiki = new Futoshiki(size);
                        break;
                    default:
                        if (i >= FutoshikiFirstLines && i < FutoshikiFirstLines + size)
                        {
                            var splitted = line.Split(';');
                            for (int column = 0; column < splitted.Length; ++column)
                            {
                                fustoshiki.AddItem(row, column, int.Parse(splitted[column]));
                            }
                            ++row;
                        }

                        if(i>= FutoshikiFirstLines +1+ size)
                        {
                            var splitted = line.Split(';');
                            int firstNumber = int.Parse(splitted[0].Substring(1));
                            int secondNumber = int.Parse(splitted[1].Substring(1));
                            
                            fustoshiki.constraints.Add(new FutoshikiConstraint(line[0] - 65, firstNumber - 1, line[3] - 65, secondNumber - 1));
                        }
                        break;
                }
                ++i;
            }
            return fustoshiki;
        }

        public ScyScrapper ReadSkyscrapperFile()
        {
            ScyScrapper scyscrapper = null;
            var lines = File.ReadLines(Path);
            int length = lines.Count();
            int i = 1;
            int size = 0;
            foreach (var line in lines)
            {
                switch (i)
                {
                    case 1:
                        size = int.Parse(line);
                        scyscrapper = new ScyScrapper(size);
                        break;
                    default:
                        if (i >= ScyscrapperFirstLines)
                        {
                            var splitted = line.Split(';');

                            switch(splitted[0])
                            {
                                case "G":
                                    for(int j=1;j<=scyscrapper.Size;++j)
                                    {
                                        scyscrapper.scyscrapper[0, j] = new ScyscrapperItem(int.Parse(splitted[j]));
                                    }
                                    break;

                                case "D":
                                    for (int j = 1; j <=scyscrapper.Size; ++j)
                                    {
                                        scyscrapper.scyscrapper[scyscrapper.Size+1, j] = new ScyscrapperItem(int.Parse(splitted[j]));
                                    }
                                    break;

                                case "L":
                                    for (int j = 1; j <=scyscrapper.Size; ++j)
                                    {
                                        scyscrapper.scyscrapper[j, 0] = new ScyscrapperItem(int.Parse(splitted[j]));
                                    }
                                    break;

                                case "P":
                                    for (int j = 1; j <= scyscrapper.Size; ++j)
                                    {
                                        scyscrapper.scyscrapper[j, scyscrapper.Size+1] = new ScyscrapperItem(int.Parse(splitted[j]));
                                    }
                                    break;
                            }
                        }
                        
                        break;
                }


                ++i;
            }
            return scyscrapper;
        }
    }
}
