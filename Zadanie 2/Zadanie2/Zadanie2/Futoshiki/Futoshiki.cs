﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Zadanie2
{
    public class Futoshiki
    {
        public int Size { get; set; }
        public FutoshikiItem[,] futoshiki { get; set; } = null;
        public List<FutoshikiConstraint> constraints { get; set; } = new List<FutoshikiConstraint>();

        public List<Tuple<int, int, int>> sortedVariables = new List<Tuple<int, int, int>>();

        public static List<Futoshiki> CorrectResults = new List<Futoshiki>();
        public static int wglebien = 0;
        public static int przypisan = 0;

        public static int przypierwszejwglebien = 0;
        public static int przypierwszejprzypisan = 0;
        public static long czasdopierwszego = 0;
        public static Stopwatch stopwatch = new Stopwatch();

        public static void ClearData()
        {
            CorrectResults.Clear();
            wglebien = 0;
            przypisan = 0;
            przypierwszejwglebien=0;
            przypierwszejprzypisan = 0;
        }

        public void BackTracking(Futoshiki f)
        {
            for (int i = 0; i < Size; ++i)
            {
                for (int j = 0; j < Size; ++j)
                {
                    if (f.futoshiki[i, j].Value == 0)
                    {
                        for (int p = 0; p < f.futoshiki[i, j].PossiblyValues.Count; ++p)
                        {
                            Futoshiki next = new Futoshiki(f);
                            next.futoshiki[i, j].Value = next.futoshiki[i, j].PossiblyValues[p];
                            ++przypisan;
                            if (next.Correct(i, j))
                            {
                                ++wglebien;
                                BackTracking(next);
                            }

                            if (next.Correct())
                            {
                                stopwatch.Stop();
                                czasdopierwszego = stopwatch.ElapsedMilliseconds;
                                stopwatch.Start();
                                CorrectResults.Add(new Futoshiki(next));
                                if (przypierwszejprzypisan == 0)
                                {
                                    przypierwszejprzypisan = przypisan;
                                    przypierwszejwglebien = wglebien;
                                }
                            }
                            else
                                if (p == f.futoshiki[i, j].PossiblyValues.Count - 1)
                                return;
                        }
                    }
                }
            }
        }
        public void BackTrackingWithBestValue(Futoshiki f)
        {
            for (int i = 0; i < Size; ++i)
            {
                for (int j = 0; j < Size; ++j)
                {
                    if (f.futoshiki[i, j].Value == 0)
                    {
                        List<Tuple<int, int>> possiblyValues = new List<Tuple<int, int>>();
                        for (int v = 0; v < f.futoshiki[i, j].PossiblyValues.Count; ++v)
                        {
                            possiblyValues.Add(new Tuple<int, int>(f.futoshiki[i, j].PossiblyValues[v], f.CountExcludingValues(i, j, f.futoshiki[i, j].PossiblyValues[v])));
                        }
                        possiblyValues.Sort((first, second) => first.Item2.CompareTo(second.Item2));
                        foreach (var value in possiblyValues)
                        {
                            Futoshiki next = new Futoshiki(f);
                            next.futoshiki[i, j].Value = value.Item1;
                            ++przypisan;
                            next.RemovePossibleValuesForItem(i,j, next.futoshiki[i,j].Value);
                            if (next.Correct(i, j))
                            {
                                ++wglebien;
                                BackTrackingWithBestValue(next);
                            }
                            if (next.Correct())
                            {
                                stopwatch.Stop();
                                czasdopierwszego = stopwatch.ElapsedMilliseconds;
                                stopwatch.Start();
                                CorrectResults.Add(new Futoshiki(next));
                                if (przypierwszejprzypisan == 0)
                                {
                                    przypierwszejprzypisan = przypisan;
                                    przypierwszejwglebien = wglebien;
                                }
                            }
                            else
                                if (value == possiblyValues.Last())
                                return;
                        }
                    }
                }
            }
        }

        public void ForwardCheckingNormal(Futoshiki f)
        {
            for (int i = 0; i < Size; ++i)
            {
                for (int j = 0; j < Size; ++j)
                {
                    if (f.futoshiki[i, j].Value == 0)
                    {
                        for (int p = 0; p < f.futoshiki[i, j].PossiblyValues.Count; ++p)
                        {
                            Futoshiki next = new Futoshiki(f);
                            next.futoshiki[i, j].Value = next.futoshiki[i, j].PossiblyValues[p];
                            ++przypisan;
                            next.RemovePossibleValuesForItem(i, j, next.futoshiki[i, j].Value);
                            if (next.ContainsMinimumOne() && next.Correct(i, j))
                            {
                                ++wglebien;
                                ForwardCheckingNormal(next);
                            }
                            if (next.Correct())
                            {
                                stopwatch.Stop();
                                czasdopierwszego = stopwatch.ElapsedMilliseconds;
                                stopwatch.Start();
                                CorrectResults.Add(new Futoshiki(next));
                                if (przypierwszejprzypisan == 0)
                                {
                                    przypierwszejprzypisan = przypisan;
                                    przypierwszejwglebien = wglebien;
                                }
                            }
                            else
                                if (p >= f.futoshiki[i, j].PossiblyValues.Count - 1)
                                return;
                        }
                    }
                }
            }
        }

        public void ForwardCheckingWithBestValue(Futoshiki f)
        {
            for (int i = 0; i < Size; ++i)
            {
                for (int j = 0; j < Size; ++j)
                {
                    if (f.futoshiki[i, j].Value == 0)
                    {
                        List<Tuple<int, int>> possiblyValues = new List<Tuple<int, int>>();
                        for (int v = 0; v < f.futoshiki[i, j].PossiblyValues.Count; ++v)
                        {
                            possiblyValues.Add(new Tuple<int, int>(f.futoshiki[i, j].PossiblyValues[v], f.CountExcludingValues(i, j, f.futoshiki[i, j].PossiblyValues[v])));
                        }
                        possiblyValues.Sort((first, second) => first.Item2.CompareTo(second.Item2));
                        foreach (var value in possiblyValues)
                        {
                            Futoshiki next = new Futoshiki(f);
                            next.futoshiki[i, j].Value = value.Item1;
                            ++przypisan;
                            next.RemovePossibleValuesForItem(i, j, next.futoshiki[i, j].Value);
                            if (next.ContainsMinimumOne() && next.Correct(i, j))
                            {
                                ++wglebien;
                                ForwardCheckingWithBestValue(next);
                            }

                            if (next.Correct())
                            {
                                stopwatch.Stop();
                                czasdopierwszego = stopwatch.ElapsedMilliseconds;
                                stopwatch.Start();
                                CorrectResults.Add(new Futoshiki(next));
                                if (przypierwszejprzypisan == 0)
                                {
                                    przypierwszejprzypisan = przypisan;
                                    przypierwszejwglebien = wglebien;
                                }
                            }
                            else
                                if (value == possiblyValues.Last())
                                return;
                        }
                    }
                }
            }
        }
        public void CountVariables(Futoshiki f)
        {
            f.sortedVariables.Clear();
            for (int a = 0; a < Size; ++a)
                for (int b = 0; b < Size; ++b)
                {
                    if (f.futoshiki[a, b].Value == 0)
                        f.sortedVariables.Add(new Tuple<int, int, int>(a, b, f.futoshiki[a, b].PossiblyValues.Count));
                }
        }

        public void ForwardCheckingWithVariables(Futoshiki f)
        {
            CountVariables(f);
            f.sortedVariables.Sort((first, second) => first.Item3.CompareTo(second.Item3));
            var current = f.sortedVariables.FirstOrDefault();
            if (current == null)
                return;
            
                for (int p = 0; p < f.futoshiki[current.Item1, current.Item2].PossiblyValues.Count; ++p)
                {
                    Futoshiki next = new Futoshiki(f);
                    next.futoshiki[current.Item1, current.Item2].Value = next.futoshiki[current.Item1, current.Item2].PossiblyValues[p];
                    ++przypisan;
                    next.sortedVariables.RemoveAll(x=>x.Item1==current.Item1 && x.Item2==current.Item2);

                    next.RemovePossibleValuesForItem(current.Item1, current.Item2, next.futoshiki[current.Item1, current.Item2].Value);
                    if (next.ContainsMinimumOne() && next.Correct(current.Item1, current.Item2))
                    {
                        ++wglebien;
                        ForwardCheckingWithVariables(next);
                    }
                    if (next.Correct())
                    {
                        stopwatch.Stop();
                        czasdopierwszego = stopwatch.ElapsedMilliseconds;
                        stopwatch.Start();
                        CorrectResults.Add(new Futoshiki(next));
                        if (przypierwszejprzypisan == 0)
                        {
                            przypierwszejprzypisan = przypisan;
                            przypierwszejwglebien = wglebien;
                        }
                    }
                    else
                        if (p >= f.futoshiki[current.Item1, current.Item2].PossiblyValues.Count - 1)
                        return;
                }
                
        }

        public void BackTrackingWithVariables(Futoshiki f)
        {
            CountVariables(f);
            f.sortedVariables.Sort((first, second) => first.Item3.CompareTo(second.Item3));
            var current = f.sortedVariables.FirstOrDefault();
            while (current != null)
            {

                for (int p = 0; p < f.futoshiki[current.Item1, current.Item2].PossiblyValues.Count; ++p)
                {
                    Futoshiki next = new Futoshiki(f);
                    next.futoshiki[current.Item1, current.Item2].Value = next.futoshiki[current.Item1, current.Item2].PossiblyValues[p];
                    ++przypisan;
                    next.sortedVariables.RemoveAll(x => x.Item1 == current.Item1 && x.Item2 == current.Item2);

                    next.RemovePossibleValuesForItem(current.Item1, current.Item2, next.futoshiki[current.Item1, current.Item2].Value);
                    if (next.Correct(current.Item1, current.Item2))
                    {
                        ++wglebien;
                        BackTrackingWithVariables(next);
                    }
                    if (next.Correct())
                    {
                        stopwatch.Stop();
                        czasdopierwszego = stopwatch.ElapsedMilliseconds;
                        stopwatch.Start();
                        CorrectResults.Add(new Futoshiki(next));
                        if (przypierwszejprzypisan == 0)
                        {
                            przypierwszejprzypisan = przypisan;
                            przypierwszejwglebien = wglebien;
                        }
                    }
                    else
                        if (p >= f.futoshiki[current.Item1, current.Item2].PossiblyValues.Count - 1)
                        return;
                }
                f.sortedVariables.Remove(current);
                current = f.sortedVariables.FirstOrDefault();
            }
        }

        public void ForwardCheckingWithVariablesAndValues(Futoshiki f)
        {
            CountVariables(f);
            f.sortedVariables.Sort((first, second) => first.Item3.CompareTo(second.Item3));
            var current = f.sortedVariables.FirstOrDefault();
            while (current != null)
            {
                List<Tuple<int, int>> possiblyValues = new List<Tuple<int, int>>();
                for (int v = 0; v < f.futoshiki[current.Item1, current.Item2].PossiblyValues.Count; ++v)
                {
                    possiblyValues.Add(new Tuple<int, int>(f.futoshiki[current.Item1, current.Item2].PossiblyValues[v], f.CountExcludingValues(current.Item1, current.Item2, f.futoshiki[current.Item1, current.Item2].PossiblyValues[v])));
                }
                possiblyValues.Sort((first, second) => first.Item2.CompareTo(second.Item2));
                foreach (var value in possiblyValues)
                {
                    Futoshiki next = new Futoshiki(f);
                    next.futoshiki[current.Item1, current.Item2].Value = value.Item1;
                    ++przypisan;
                    next.sortedVariables.RemoveAll(x => x.Item1 == current.Item1 && x.Item2 == current.Item2);

                    next.RemovePossibleValuesForItem(current.Item1, current.Item2, next.futoshiki[current.Item1, current.Item2].Value);
                    if (next.ContainsMinimumOne() && next.Correct(current.Item1, current.Item2))
                    {
                        ++wglebien;
                        ForwardCheckingWithVariablesAndValues(next);
                    }
                    if (next.Correct())
                    {
                        CorrectResults.Add(new Futoshiki(next));
                        if (przypierwszejprzypisan == 0)
                        {
                            przypierwszejprzypisan = przypisan;
                            przypierwszejwglebien = wglebien;
                        }
                    }
                    else
                        if (value == possiblyValues.Last())
                        return;
                }
                f.sortedVariables.Remove(current);
                current = f.sortedVariables.FirstOrDefault();
            }
        }

        public void BackTrackingWithVariablesAndValues(Futoshiki f)
        {
            CountVariables(f);
            f.sortedVariables.Sort((first, second) => first.Item3.CompareTo(second.Item3));
            var current = f.sortedVariables.FirstOrDefault();
            while (current != null)
            {
                List<Tuple<int, int>> possiblyValues = new List<Tuple<int, int>>();
                for (int v = 0; v < f.futoshiki[current.Item1, current.Item2].PossiblyValues.Count; ++v)
                {
                    possiblyValues.Add(new Tuple<int, int>(f.futoshiki[current.Item1, current.Item2].PossiblyValues[v], f.CountExcludingValues(current.Item1, current.Item2, f.futoshiki[current.Item1, current.Item2].PossiblyValues[v])));
                }
                possiblyValues.Sort((first, second) => first.Item2.CompareTo(second.Item2));
                foreach (var value in possiblyValues)
                {
                    Futoshiki next = new Futoshiki(f);
                    next.futoshiki[current.Item1, current.Item2].Value = value.Item1;
                    ++przypisan;
                    next.sortedVariables.RemoveAll(x => x.Item1 == current.Item1 && x.Item2 == current.Item2);

                    next.RemovePossibleValuesForItem(current.Item1, current.Item2, next.futoshiki[current.Item1, current.Item2].Value);
                    if (next.Correct(current.Item1, current.Item2))
                    {
                        ++wglebien;
                        BackTrackingWithVariablesAndValues(next);
                    }
                    if (next.Correct())
                    {
                        CorrectResults.Add(new Futoshiki(next));
                        if (przypierwszejprzypisan == 0)
                        {
                            przypierwszejprzypisan = przypisan;
                            przypierwszejwglebien = wglebien;
                        }
                    }
                    else
                        if (value == possiblyValues.Last())
                        return;
                }
                f.sortedVariables.Remove(current);
                current = f.sortedVariables.FirstOrDefault();
            }
        }


        public bool ContainsMinimumOne()
        {
            for (int i = 0; i < Size; ++i)
                for (int j = 0; j < Size; ++j)
                {
                    if (futoshiki[i, j].Value == 0)
                    {
                        if (futoshiki[i, j].PossiblyValues.Count == 0)
                            return false;
                    }
                }
            return true;
        }

        public void RemovePossibleValuesForItem(int row, int column, int value)
        {
            for (int i = 0; i < Size; ++i)
            {
                if (futoshiki[i, column].Value == 0)
                    futoshiki[i, column].PossiblyValues.Remove(value);
            }

            for (int i = 0; i < Size; ++i)
            {
                if (futoshiki[row, i].Value == 0)
                    futoshiki[row, i].PossiblyValues.Remove(value);
            }

            //wykluczajace contrainty
            foreach (var constraint in constraints)
            {
                if (constraint.FirstColumn == column && constraint.FirstRow == row)
                {
                    var toRemove = futoshiki[constraint.SecondRow, constraint.SecondColumn].PossiblyValues.Where(x => x <= value).ToList();
                    foreach (var removing in toRemove)
                        futoshiki[constraint.SecondRow, constraint.SecondColumn].PossiblyValues.Remove(removing);
                    
                }
                if (constraint.SecondColumn == column && constraint.SecondRow == row)
                {
                    var toRemove = futoshiki[constraint.SecondRow, constraint.SecondColumn].PossiblyValues.Where(x => x >= value).ToList();
                    foreach (var removing in toRemove)
                        futoshiki[constraint.SecondRow, constraint.SecondColumn].PossiblyValues.Remove(removing);
                }
            }

        }

        public void FillPossibleValues()
        {
            for (int i = 0; i < Size; ++i)
                for (int j = 0; j < Size; ++j)
                {
                    if (futoshiki[i, j].Value == 0)
                    {
                        
                        futoshiki[i, j].PossiblyValues.Clear();
                        for (int times = 1; times <= Size; ++times)
                            futoshiki[i, j].PossiblyValues.Add(times);
                    }

                }
            for (int i = 0; i < Size; ++i)
                for (int j = 0; j < Size; ++j)
                {
                    if (futoshiki[i, j].Value != 0)
                    {
                        RemovePossibleValuesForItem(i, j, futoshiki[i, j].Value);
                    }
                    if (futoshiki[i, j].Value == 0)
                        sortedVariables.Add(new Tuple<int, int, int>(i, j, futoshiki[i, j].PossiblyValues.Count));
                }
        }

        public override string ToString()
        {
            string s = "<table>";
            for (int i = 0; i < Size; ++i)
            {
                s += "<tr>";
                for (int j = 0; j < Size; ++j)
                {
                    s += "<td>"+futoshiki[i, j].Value + "</td>";
                }
                s += "</tr>\n ";
            }
            s += "</table>";
            return s;
        }

        public Futoshiki(Futoshiki copyFrom)
        {
            Size = copyFrom.Size;
            futoshiki = new FutoshikiItem[Size, Size];
            for (int a = 0; a < Size; ++a)
                for (int b = 0; b < Size; ++b)
                {
                    futoshiki[a, b] = new FutoshikiItem(copyFrom.futoshiki[a, b]);
                    if (futoshiki[a, b].Value == 0)
                        sortedVariables.Add(new Tuple<int, int, int>(a, b, futoshiki[a, b].PossiblyValues.Count));
                }
            foreach (var constraint in copyFrom.constraints)
                constraints.Add(new FutoshikiConstraint(constraint));
            
        }

        public Futoshiki(int size)
        {
            Size = size;
            futoshiki = new FutoshikiItem[size, size];
            for (int a = 0; a < size; ++a)
                for (int b = 0; b < size; ++b)
                    futoshiki[a, b] = new FutoshikiItem();
                
        }

        public void AddItem(int row, int column, int value)
        {
            futoshiki[row, column].Value = value;
        }

        public int CountExcludingValues(int row, int column, int value)
        {
            int times = 0;
            

            //wykluczajace contrainty
            foreach (var constraint in constraints)
            {
                if (constraint.FirstColumn == column && constraint.FirstRow == row)
                {
                    times += futoshiki[constraint.SecondRow, constraint.SecondColumn].PossiblyValues.Count(x => x <= value);
                }
                if (constraint.SecondColumn == column && constraint.SecondRow == row)
                {
                    times += futoshiki[constraint.SecondRow, constraint.SecondColumn].PossiblyValues.Count(x => x >= value);
                }
            }

            //if(times==0)
            //{
            //    for (int i = 0; i < Size; ++i)
            //    {
            //        if (futoshiki[i, column].Value == 0 && futoshiki[i, column].PossiblyValues.Contains(value))
            //            ++times;

            //        if (futoshiki[row, i].Value == 0 && futoshiki[row, i].PossiblyValues.Contains(value))
            //            ++times;
            //    }
            //}

            return times;
        }

        public FutoshikiItem Get(int row, int column)
        {
            return futoshiki[row, column];
        }

        public bool CorrectConstraints()
        {
            foreach (var constraint in constraints)
            {
                if (futoshiki[constraint.FirstRow, constraint.FirstColumn].Value > futoshiki[constraint.SecondRow, constraint.SecondColumn].Value)
                    return false;
            }
            return true;
        }

        public bool Correct()
        {
            foreach (var constraint in constraints)
            {
                if (!(futoshiki[constraint.FirstRow, constraint.FirstColumn].Value < futoshiki[constraint.SecondRow, constraint.SecondColumn].Value))
                    return false;
            }

            for (int i = 0; i < Size; ++i)
            {
                List<int> rows = new List<int>();
                List<int> columns = new List<int>();
                for (int j = 0; j < Size; ++j)
                {
                    if (futoshiki[i, j].Value == 0 || futoshiki[j, i].Value == 0)
                        return false;
                    rows.Add(futoshiki[i, j].Value);
                    columns.Add(futoshiki[j, i].Value);
                }
                if ((rows.Distinct().Count() != Size) || (columns.Distinct().Count() != Size))
                    return false;
            }
            return true;
        }

        public bool Correct(int row, int column)
        {
            List<int> columns = new List<int>();
            for (int i = 0; i < Size; ++i)
            {
                if (futoshiki[i, column].Value != 0)
                {
                    if (columns.Contains(futoshiki[i, column].Value))
                        return false;
                    columns.Add(futoshiki[i, column].Value);
                }

            }

            List<int> rows = new List<int>();
            for (int i = 0; i < Size; ++i)
            {
                if (futoshiki[row, i].Value != 0)
                {
                    if (rows.Contains(futoshiki[row, i].Value))
                        return false;
                    rows.Add(futoshiki[row, i].Value);
                }

            }
            foreach (var constraint in constraints)
            {
                if (futoshiki[constraint.FirstRow, constraint.FirstColumn].Value != 0 && futoshiki[constraint.SecondRow, constraint.SecondColumn].Value != 0)
                    if (futoshiki[constraint.FirstRow, constraint.FirstColumn].Value > futoshiki[constraint.SecondRow, constraint.SecondColumn].Value)
                        return false;
            }
            return true;
        }

        public void AddConstraint(int firstRow, int firstColumn, int secondRow, int secondColumn)
        {
            constraints.Add(new FutoshikiConstraint(firstRow, firstColumn, secondRow, secondColumn));
        }
    }
}
