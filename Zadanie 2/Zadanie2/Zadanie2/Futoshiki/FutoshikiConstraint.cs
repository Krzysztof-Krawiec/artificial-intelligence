﻿namespace Zadanie2
{
    public class FutoshikiConstraint
    {
        public FutoshikiConstraint(int firstRow, int firstColumn,int secondRow, int secondColumn)
        {
            FirstColumn = firstColumn;
            FirstRow = firstRow;
            SecondColumn = secondColumn;
            SecondRow = secondRow;
        }

        public int FirstColumn { get; set; }
        public int FirstRow { get; set; }
        public int SecondColumn { get; set; }
        public int SecondRow { get; set; }

        public FutoshikiConstraint(FutoshikiConstraint copyFrom)
        {
            FirstColumn = copyFrom.FirstColumn;
            FirstRow = copyFrom.FirstRow;
            SecondColumn = copyFrom.SecondColumn;
            SecondRow = copyFrom.SecondRow;
        }
    }
}
