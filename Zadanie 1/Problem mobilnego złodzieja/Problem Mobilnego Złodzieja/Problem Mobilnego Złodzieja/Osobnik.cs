﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Problem_Mobilnego_Złodzieja
{
    public class Osobnik
    {
        public List<int> nodes = new List<int>();
        public List<ItemToPack> selectedItems = new List<ItemToPack>();
        public double Fitness { get; set; }
        public bool CalculateFitness { get; set; }
        public Osobnik()
        {
            CalculateFitness = true;
        }

        public Osobnik(Osobnik osobnik)
        {
            foreach (var node in osobnik.nodes)
                nodes.Add(node);
            foreach(var item in osobnik.selectedItems)
                selectedItems.Add(new ItemToPack(item));
            Fitness=osobnik.Fitness;
            CalculateFitness = osobnik.CalculateFitness;
        }

        public void CountOsobnikFitness()
        {
            if (CalculateFitness)
            {
                FillKnapsackWithItems();
                Fitness = GetKnapsackProfit() - CountFunctionFXY();
                CalculateFitness = false;
            }
        }

        public void GenerateNodes()
        {
            CommonFunctions.defaultNodes.Shuffle<int>();
            foreach (var node in CommonFunctions.defaultNodes)
            {
                nodes.Add(node);
            }
            CalculateFitness = true;
        }

        private double GetTotalDistance()
        {
            var enumerator = nodes.GetEnumerator();
            enumerator.MoveNext();
            double distance=0;
            for (int i = 0; i < nodes.Count - 1; ++i)
            {
                var currentNode = enumerator.Current;
                enumerator.MoveNext();
                var nextNode = enumerator.Current;            
                distance += CommonFunctions.GetDistance(currentNode, nextNode);
            }

            var lastNode = nodes.Last();
            var firstNode = nodes.First();

            distance += CommonFunctions.GetDistance(lastNode, firstNode);
            return distance;
        }

        private double GetKnapsackProfit()
        {
            double res = 0;
            foreach (var item in selectedItems)
                res += item.Profit;
            return res;
        }

        //KNP
        public void FillKnapsackWithItems()
        {
            selectedItems.Clear();
            int currentWeight = 0;
            double totalDistance = GetTotalDistance();
            double remainingDistance = totalDistance;
            double currentRange = remainingDistance / totalDistance;

            var enumerator = nodes.GetEnumerator();
            enumerator.MoveNext();

            for (int i = 0; i < nodes.Count; ++i)
            {
                var currentNode = enumerator.Current;
                enumerator.MoveNext();
                var nextNode = enumerator.Current;

                foreach (var item in CommonFunctions.itemsToPackInCities[currentNode])
                {
                    double temp = item.Profit - remainingDistance / CountCurrentSpeed(CommonFunctions.informations.MaxSpeed, CommonFunctions.informations.MinSpeed, item.Weight, CommonFunctions.informations.CapacityOfKnapsack) + item.Weight / item.Profit;
                    //double temp = remainingDistance / CountCurrentSpeed(CommonFunctions.informations.MaxSpeed, CommonFunctions.informations.MinSpeed, item.Weight, CommonFunctions.informations.CapacityOfKnapsack) + item.Weight/item.Profit;
                    CommonFunctions.itemsToPack.Find(x => x.ID == item.ID).CurrentRange = temp;// (double)item.Profit/ (double)item.Weight;
                }

                if (i != nodes.Count - 1)
                {
                    remainingDistance -= CommonFunctions.GetDistance(currentNode, nextNode);
                    
                }
            }

            List<ItemToPack> sorteditemsToPack = CommonFunctions.itemsToPack.OrderByDescending(x=>x.CurrentRange).ToList();
            currentWeight = 0;
            foreach (var item in sorteditemsToPack)
            {
                if (item.Weight + currentWeight <= CommonFunctions.informations.CapacityOfKnapsack)
                {
                    selectedItems.Add(new ItemToPack(item));
                    currentWeight += item.Weight;
                }
            }
        }


        private double CountCurrentSpeed(double vMax, double vMin, int currentWeight, int maxWeight)
        {
            return vMax - currentWeight * (vMax - vMin) / maxWeight;
        }

        private int GetWeightForNode(int currentNode)
        {
            double s = selectedItems.Sum(x=>x.Weight);
            int weight = 0;
            var finded = selectedItems.Where(x => x.AvaiableInCity == currentNode).ToList();
            if (finded.Count > 0)
                foreach (var item in finded)
                    weight += item.Weight;
            return weight;

        }

        //function f(x,y)
        public double CountFunctionFXY()
        {
            var enumerator = nodes.GetEnumerator();
            enumerator.MoveNext();
            int currentWeight = 0;
            double result = 0;
            double distance, currentSpeed;
            for (int i =0;i<nodes.Count-1;++i)
            {
                var currentNode = enumerator.Current;
                enumerator.MoveNext();
                var nextNode = enumerator.Current;

                currentWeight += GetWeightForNode(currentNode);
                
                distance = CommonFunctions.GetDistance(currentNode, nextNode);
                currentSpeed = CountCurrentSpeed(CommonFunctions.informations.MaxSpeed, CommonFunctions.informations.MinSpeed, currentWeight, CommonFunctions.informations.CapacityOfKnapsack);
                result += distance / currentSpeed;
            }

            var lastNode = nodes.Last();
            var firstNode = nodes.First();

            currentWeight += GetWeightForNode(lastNode);
            distance = CommonFunctions.GetDistance(lastNode, firstNode);
            currentSpeed = CountCurrentSpeed(CommonFunctions.informations.MaxSpeed, CommonFunctions.informations.MinSpeed, currentWeight, CommonFunctions.informations.CapacityOfKnapsack);
            result += distance / currentSpeed;

            return result;
        }


        public void Mutate()
        {
            Random random = new Random();
              
            int firstNode = random.Next(0, nodes.Count);
            int secondNode = random.Next(0, nodes.Count);

            int tmp = nodes[firstNode];
            nodes[firstNode] = nodes[secondNode];
            nodes[secondNode] = tmp;
            CalculateFitness = true;
        }
    }
}
