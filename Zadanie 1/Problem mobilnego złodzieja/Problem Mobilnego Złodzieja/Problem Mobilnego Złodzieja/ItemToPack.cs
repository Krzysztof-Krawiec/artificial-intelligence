﻿namespace Problem_Mobilnego_Złodzieja
{
    public class ItemToPack
    {
        public ItemToPack()
        {
        }

        public ItemToPack(int iD, int profit, int weight, int avaiableInCity)
        {
            ID = iD;
            Profit = profit;
            Weight = weight;
            AvaiableInCity = avaiableInCity;
        }

        public ItemToPack(ItemToPack itemToPack)
        {
            ID = itemToPack.ID;
            Profit = itemToPack.Profit;
            Weight = itemToPack.Weight;
            AvaiableInCity = itemToPack.AvaiableInCity;
        }

        public int ID { get; set; }
        public int Profit { get; set; }
        public int Weight { get; set; }
        public int AvaiableInCity { get; set; }
        public double CurrentRange { get; set; }

    }
}