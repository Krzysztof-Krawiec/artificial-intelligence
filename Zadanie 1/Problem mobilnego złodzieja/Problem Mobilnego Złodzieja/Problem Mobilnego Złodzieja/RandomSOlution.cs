﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_Mobilnego_Złodzieja
{
    public static class RandomSolution
    {
        public static double RandomCount()
        {
            Osobnik solution = new Osobnik();
            solution.GenerateNodes();
            solution.FillKnapsackWithItems();
            solution.CountOsobnikFitness();
            return solution.Fitness;
        }

        public static Tuple<double,double,double> CountSolutions(int repeatTimes)
        {
            double sumOfResults=0;
            double minResult = double.MaxValue;
            double maxResult = double.MinValue;
            for(int i=0;i<repeatTimes;++i)
            {
                double currentResult = RandomCount();
                if (minResult > currentResult)
                    minResult = currentResult;
                if (maxResult < currentResult)
                    maxResult = currentResult;
                sumOfResults += currentResult;
            }
            return new Tuple<double, double, double>(minResult, maxResult, sumOfResults / repeatTimes);
        }

    }
}
