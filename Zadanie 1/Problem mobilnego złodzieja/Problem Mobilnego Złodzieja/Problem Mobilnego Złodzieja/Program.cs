﻿using System;
using System.Collections.Generic;

namespace Problem_Mobilnego_Złodzieja
{
    class Program
    {
        const string knn ="knnDobry.txt";
        const string genetic = "ruletka1";
        const string rand = "randDobry.txt";
        const string data =  "medium_0.ttp";

        const int populationSize = 100;
        const int generations = 100;
        const double probabilityOfCrosover = 0.6;
        const double probabilityOfMutation = 0.01;
        const int timesToRepeatWholeProcess = 3;



        static void Main(string[] args)
        {
            CommonFunctions.ReadFile(data);
            CommonFunctions.CountDimensions();

            //Osobnik o = new Osobnik();
            //o.nodes.Add(2);
            //o.nodes.Add(1);
            //o.nodes.Add(5);
            //o.nodes.Add(6);
            //o.nodes.Add(4);
            //o.nodes.Add(10);
            //o.nodes.Add(9);
            //o.nodes.Add(8);
            //o.nodes.Add(3);
            //o.nodes.Add(7);
            //o.FillKnapsackWithItems();
            //o.CountOsobnikFitness();
            //var a = 5;


            //SaveToFile(KNNSolution.KNNCount().ToString(), knn);
            //Tuple<double, double, double> randomResult = RandomSolution.CountSolutions(10);
            //SaveToFile(randomResult.Item1.ToString() + " " + randomResult.Item2.ToString() + " " + randomResult.Item3.ToString(), rand);

            StartGeneticAlgorithm();

            //Console.ReadLine();
        }

        public static void StartGeneticAlgorithm()
        {
            List<Result> bests = new List<Result>();
            for (int i = 0; i < generations; ++i)
            {
                bests.Add(new Result());
            }

            for (int geneticTimes = 0; geneticTimes < timesToRepeatWholeProcess; ++geneticTimes)
            {
                
                List<Osobnik>[] population = new List<Osobnik>[2];
                population[0] = new List<Osobnik>();
                for (int i = 0; i < populationSize; ++i)
                {
                    Osobnik o = new Osobnik();
                    o.GenerateNodes();
                    population[0].Add(o);
                }
                CommonFunctions.Evaluation(population[0]);
                int last = 0;
                int next = 1;
                //bests[0].AddToResult(GetBest(population[0]), GetWorst(population[0]), GetAverage(population[0]));
                for (int i = 0; i < generations - 1; ++i)
                {
                    population[next] = CommonFunctions.Selection(population[last], populationSize, TypeOfSelection.Tournament);
                    bests[i].AddToResult(GetBest(population[next]), GetWorst(population[next]), GetAverage(population[next]));
                    population[next] = CommonFunctions.Crossover(population[next], probabilityOfCrosover);
                    CommonFunctions.Mutation(population[next], probabilityOfMutation);
                    CommonFunctions.Evaluation(population[next]);
                    
                    last = next;
                    next = next == 1 ? 0 : 1;
                }

                
            }
            SaveToFile(bests, genetic, timesToRepeatWholeProcess);
        }

        public static void SaveToFile(List<Result> bests, string path, int timesToRepeatWholeProcess)
        {
            string p = path+ ".txt";
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(p))
            {
                int i = 0;
                foreach (var item in bests)
                {
                    item.CountResult(timesToRepeatWholeProcess);
                    file.WriteLine(i + " " + item.item1 + " " + item.item2 + " " + item.item3 + " " + item.Odchylenie1 + " " + item.Odchylenie2 + " " + item.Odchylenie3);
                    
                    ++i;
                }
            }
        }

        public static void SaveToFile(string result, string path)
        {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@path))
            {
                    file.WriteLine(1 + " " + result);   
            }
        }

        public static double GetBest(List<Osobnik> population)
        {
            double best = double.MinValue;
            foreach(var a in population)
            {
                if (a.Fitness>best)
                {
                    best = a.Fitness;
                }
            }
            return best;
        }

        public static double GetWorst(List<Osobnik> population)
        {
            double worst = double.MaxValue;
            foreach (var a in population)
            {
                if (a.Fitness < worst)
                {
                    worst = a.Fitness;
                }
            }
            return worst;
        }

        public static double GetAverage(List<Osobnik> population)
        {
            double totalFIntess = 0;
            foreach (var a in population)
            {
                totalFIntess += a.Fitness;
            }
            return totalFIntess/population.Count;
        }

       


        
    }
}
