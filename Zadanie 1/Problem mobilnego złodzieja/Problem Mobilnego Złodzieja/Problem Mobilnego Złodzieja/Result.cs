﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_Mobilnego_Złodzieja
{
    public class Result
    {
        public Result()
        {
            Best = new List<double>();
            Worst = new List<double>();
            Average = new List<double>();
        }

        //public Result(double best, double worst, double average)
        //{
        //    Best = best;
        //    Worst = worst;
        //    Average = average;
        //}

        public List<double> Best { get; set; }
        public List<double> Worst { get; set; }
        public List<double> Average { get; set; }
        public double Odchylenie1 { get; set; }
        public double Odchylenie2 { get; set; }
        public double Odchylenie3 { get; set; }
        public double item1 { get; set; }
        public double item2 { get; set; }
        public double item3 { get; set; }

        public void CountResult(int divideBy)
        {
            Odchylenie1 = 0;
            Odchylenie2 = 0;
            Odchylenie3 = 0;
            item1 = 0;
            item2 = 0;
            item3 = 0;
            for(int i=0;i<Best.Count;++i)
            {
                item1 += Best[i];
                item2 += Worst[i];
                item3 += Average[i];
            }
            item1 = item1 / Best.Count;
            item2 = item2 / Best.Count;
            item3 = item3 / Best.Count;

            for(int i=0;i<Best.Count;++i)
            {
                Odchylenie1 += Math.Pow(Best[i] - item1, 2);
                Odchylenie2 += Math.Pow(Worst[i] - item2, 2);
                Odchylenie3 += Math.Pow(Average[i] - item3, 2);
            }
            Odchylenie1 = Math.Sqrt(Odchylenie1/Best.Count);
            Odchylenie2 = Math.Sqrt(Odchylenie2 / Best.Count);
            Odchylenie3 = Math.Sqrt(Odchylenie3 / Best.Count);
        }

        public void AddToResult(double best, double worst, double average)
        {
            Best.Add( best);
            Worst.Add(worst);
            Average.Add(average);
        }
    }
}
