﻿using System;
using System.Linq;

namespace Problem_Mobilnego_Złodzieja
{
    public static class KNNSolution
    {
        public static double KNNCount()
        {
            Osobnik solution = new Osobnik();
            int firstNode = 1;
            int currentNode = firstNode;
            solution.nodes.Add(firstNode);

            for(int i=0;i<CommonFunctions.informations.Dimension-1;++i)
            {
                double minDistance = double.MaxValue;
                int bestNode = 0;
                for(int nextNode=1;nextNode<=CommonFunctions.informations.Dimension;++nextNode)
                {
                    if(!solution.nodes.Contains(nextNode))
                    {
                        double distance = CommonFunctions.GetDistance(currentNode, nextNode);
                        if(distance<minDistance)
                        {
                            minDistance = distance;
                            bestNode = nextNode;
                        }
                    }
                }
                solution.nodes.Add(bestNode);
                currentNode = bestNode;
            }
            //solution.FillKnapsackWithItems();
            solution.CountOsobnikFitness();
            var t = solution.nodes;
            var tt = solution.selectedItems;
            Console.WriteLine(tt.ToArray());
            return solution.Fitness;
        }
    }
}
