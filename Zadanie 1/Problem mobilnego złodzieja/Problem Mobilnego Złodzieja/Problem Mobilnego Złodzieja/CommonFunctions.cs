﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Problem_Mobilnego_Złodzieja
{
    public enum TypeOfSelection { Ranking, Ruletka, Tournament };
    public class CommonFunctions
    {
        const int beginLines = 10;
        public static Informations informations = new Informations();
        public static double[,] tab;
        public static List<ItemToPack>[] itemsToPackInCities;
        protected static Tuple<double, double>[] tuples;
        public static List<ItemToPack> itemsToPack = new List<ItemToPack>();
        public static List<int> defaultNodes = new List<int>();

        

        public static double GetDistance(int firstNode, int secondNode)
        {
            return tab[firstNode, secondNode];
        }

        protected static string GetParameter(int param,string line)  
        {
            var splitted = line.Split('\t');
            return splitted[param];
        }

        public static void CountDimensions()
        {
            for(int i =1;i<= CommonFunctions.informations.Dimension; ++i)
            {
                for (int j = 1;j<= CommonFunctions.informations.Dimension; ++j)
                {
                    double xDifference = Math.Abs(tuples[i].Item1 - tuples[j].Item1);
                    double yDifference = Math.Abs(tuples[i].Item2 - tuples[j].Item2);
                    double difference = Math.Sqrt(xDifference * xDifference + yDifference * yDifference);
                    tab[i, j] = difference;
                }
            }
        }

        public static void Evaluation(List<Osobnik> list)
        {
            foreach(var item in list)
            {
                item.CountOsobnikFitness();
            }
        }

        public static void ReadFile(string path)
        {
            var lines = File.ReadLines(path);
            int length = lines.Count();
            int i = 0;
            int help_int= 0;
            double help_double=0;
            foreach (var line in lines)
            {
                switch (i)
                {
                    case 2:
                        int.TryParse(GetParameter(1, line), out help_int);
                        informations.Dimension = help_int;
                        tab = new double[help_int+1,help_int+1];
                        tuples = new Tuple<double, double>[help_int+1];
                        itemsToPackInCities = new List<ItemToPack>[help_int + 1];
                        for (int j = 1; j < help_int + 1; ++j)
                        {
                            itemsToPackInCities[j] = new List<ItemToPack>();
                            defaultNodes.Add(j);
                        }
                        break;
                    case 3:
                        int.TryParse(GetParameter(1, line), out help_int);
                        informations.NumberOfItems = help_int;
                        break;
                    case 4:
                        int.TryParse(GetParameter(1, line), out help_int);
                        informations.CapacityOfKnapsack = help_int;
                        break;
                    case 5:
                        double.TryParse(GetParameter(1, line), System.Globalization.NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out help_double);
                        informations.MinSpeed= help_double;
                        break;
                    case 6:
                        double.TryParse(GetParameter(1, line), System.Globalization.NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out help_double);
                        informations.MaxSpeed= help_double;
                        break;
                    case 7:
                        double.TryParse(GetParameter(1, line), System.Globalization.NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out help_double);
                        informations.RentingRatio= help_double;
                        break;
                    default:

                        if (i >= beginLines  && i< beginLines + informations.Dimension)
                        {
                            var splitted = line.Split('\t');
                            tuples[int.Parse(splitted[0])] = new Tuple<double, double>(double.Parse(splitted[1], CultureInfo.InvariantCulture),double.Parse(splitted[2], CultureInfo.InvariantCulture));
                        }
                        if (i > beginLines + informations.Dimension) 
                        {
                            var splitted = line.Split('\t');
                            itemsToPackInCities[int.Parse(splitted[3])].Add(new ItemToPack(int.Parse(splitted[0]), int.Parse(splitted[1]), int.Parse(splitted[2]), int.Parse(splitted[3])));
                            itemsToPack.Add(new ItemToPack(int.Parse(splitted[0]), int.Parse(splitted[1]), int.Parse(splitted[2]), int.Parse(splitted[3])));
                        }
                        break;
                }
                ++i;
            }
        }

        private static void GenerateFistAndSecondPosition(int lenght, ref int first, ref int second)
        {
            Random random = new Random();
            first = random.Next(0, lenght);
            second = random.Next(0, lenght);
            if (first > second)
            {
                int temp = first;
                first = second;
                second = temp;
            }
        }
        public static Osobnik CrossOX(Osobnik par1, Osobnik par2)
        {
            List<Osobnik> childs = new List<Osobnik>();
            int nodesLength = par1.nodes.Count;
            int[] child1 = new int[nodesLength];
            int[] child2 = new int[nodesLength];

            int[] parent1 = new int[nodesLength];
            int[] parent2 = new int[nodesLength];

            int position = 0;
            foreach(var item in par1.nodes)
            {
                parent1[position] = item;
                ++position;
            }
            position = 0;
            foreach (var item in par2.nodes)
            {
                parent2[position] = item;
                ++position;
            }

            int firstPosition =0, secondPosition=0;
            GenerateFistAndSecondPosition(nodesLength, ref firstPosition, ref secondPosition);

            List<int> commonSubParent1 = new List<int>();
            List<int> commonSubParent2 = new List<int>();

            for (int i= firstPosition; i<=secondPosition;++i)
            {
                child1[i] = parent1[i];
                commonSubParent1.Add(parent1[i]);
                child2[i] = parent2[i];
                commonSubParent2.Add(parent2[i]);
            }

            List<int> remainingNodesInParent1 = new List<int>();
            List<int> remainingNodesInParent2 = new List<int>();
            for (int i =1;i<=nodesLength;++i)
            {
                int candicateToRemaining = parent1[(secondPosition + i) % nodesLength];
                if (!commonSubParent2.Contains(candicateToRemaining))
                {
                    remainingNodesInParent2.Add(candicateToRemaining);
                }

                candicateToRemaining = parent2[(secondPosition + i) % nodesLength];
                if (!commonSubParent1.Contains(candicateToRemaining))
                {
                    remainingNodesInParent1.Add(candicateToRemaining);
                }
            }

            position = 1;
            foreach(var node in remainingNodesInParent1)
            {
                child1[(secondPosition + position) % nodesLength] = node;
                ++position;
            }
            position = 1;
            foreach (var node in remainingNodesInParent2)
            {
                child2[(secondPosition + position) % nodesLength] = node;
                ++position;
            }

            Osobnik os1 = new Osobnik();
            Osobnik os2 = new Osobnik();
            for(int i=0;i<nodesLength;++i)
            {
                os1.nodes.Add(child1[i]);
                os2.nodes.Add(child2[i]);
            }
            os1.CalculateFitness = true;
            os2.CalculateFitness = true;
            Random random = new Random();
            if (random.Next(0, 1)==0)
                return os1;
            else
                return os2;

        }

        public static List<Osobnik> Ranking(List<Osobnik> population, int length)
        {
            List<Osobnik> newPopultaion = new List<Osobnik>();
            var old = population.OrderByDescending(x => x.Fitness).Take(length).ToList();
            foreach (var oldPop in old)
                newPopultaion.Add(new Osobnik(oldPop));
            return newPopultaion;
        }
        public static List<Osobnik> Selection(List<Osobnik> population, int length, TypeOfSelection typeOfSelection)
        {
            switch(typeOfSelection) 
            {
                case TypeOfSelection.Ranking :
                    return Ranking(population, length);
                case TypeOfSelection.Ruletka:
                    return Ruletka(population, length);
                case TypeOfSelection.Tournament:
                    return Tournament(population, length);
                default: return null;
            }
        }

        public static List<Osobnik> Tournament(List<Osobnik> population, int populationSize)
        {
            List<Osobnik> newPopulation = new List<Osobnik>();
            Random random = new Random();

            int tournamentSize = 2;
            int selectedCount = 0;
            int i = 0;
            while(selectedCount !=populationSize)
            {
                newPopulation.Add(new Osobnik(population.GetRange(i, tournamentSize).OrderByDescending(x => x.Fitness).Take(1).First()));
                ++selectedCount;
                i+= tournamentSize;
                if(i+tournamentSize > population.Count )
                {
                    i = 0;
                    population.Shuffle<Osobnik>();
                }
            }




            //for (int times = 0; times < populationSize; ++times)
            //{
            //    List<Osobnik> players = new List<Osobnik>();
            //    List<int> indexes = new List<int>();
            //    int numerOfPlayers = 4;
            //    for (int i = 0; i < numerOfPlayers; ++i)
            //    {
            //        int index = random.Next(0, population.Count);
            //        while (indexes.Contains(index))
            //        {
            //            index = random.Next(0, population.Count);
            //        }
            //        indexes.Add(index);
            //        players.Add(new Osobnik(population[index]));
            //    }
            //    Osobnik bestPlyer = players.OrderByDescending(x => x.Fitness).First();
            //    newPopulation.Add(new Osobnik(bestPlyer));
            //}

            return newPopulation;
        }

        public static List<Osobnik> Crossover(List<Osobnik> population, double probability)
        {
            List<Osobnik> result = new List<Osobnik>();
            foreach (var item in population)
                result.Add(new Osobnik(item));
            Random random = new Random();
            double times = population.Count * probability;
            for(int i=0;i< times; ++i)
            {
                int first = random.Next(0, population.Count);
                int second = random.Next(0, population.Count);
                if (first == second)
                    continue;
                result.Add(CrossOX(population[first], population[second]));
            }
            return result;
        }
        public static void Mutation(List<Osobnik> population, double probability)
        {
            Random random = new Random();
            for (int i = 0; i < probability * population.Count; ++i)
            {
                population[random.Next(0, population.Count)].Mutate();
            }
        }
        public static List<Osobnik> Ruletka(List<Osobnik> population, int populationSize)
        {
            
            double[] fitnesses = new double[population.Count];
            double sumOfFintesses = 0;

            List<Osobnik> newPopulation = new List<Osobnik>();
            double minF = Math.Abs(population.Min(x => x.Fitness));

            int position = 0;
            foreach (var os in population)
            {
                if (position > 0)
                {
                    fitnesses[position] = fitnesses[position - 1] + (os.Fitness+minF);
                }
                else
                {
                    fitnesses[position] = fitnesses[position ] + (os.Fitness + minF) ;
                }
                sumOfFintesses += (os.Fitness + minF);
                ++position;
            }
            
            
            Random random = new Random();
            for (int times = 0; times < populationSize; ++times)
            {
                int rand = random.Next(0, (int)sumOfFintesses);
                bool finded = false;
                double ost = 0;
                for (int i = 0; i < fitnesses.Length - 1; ++i)
                {
                    if (rand < fitnesses[i + 1])
                    {
                        finded = true;
                        if (rand > ost)
                            newPopulation.Add(new Osobnik(population[i + 1]));
                        else
                            newPopulation.Add(new Osobnik(population[i]));
                        i = fitnesses.Length - 1;
                    }
                    ost = fitnesses[i];
                }
                if (!finded)
                {
                    newPopulation.Add(new Osobnik(population[population.Count - 1]));
                }
            }

            return newPopulation;
        }
    }
    public static class ListExtend
    {
        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

    }
}
