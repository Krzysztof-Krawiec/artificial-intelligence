﻿namespace Problem_Mobilnego_Złodzieja
{
    public class Informations
    {
        public int Dimension { get; set; }
        public int NumberOfItems{ get; set; }
        public int CapacityOfKnapsack { get; set; }
        public double MinSpeed { get; set; }
        public double MaxSpeed { get; set; }
        public double RentingRatio { get; set; }
    }

    
}
