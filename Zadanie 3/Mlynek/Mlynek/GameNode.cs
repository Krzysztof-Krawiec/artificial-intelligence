﻿using System.Collections.Generic;

namespace Mlynek
{
    public class GameNode
    {
        public Game _game;
        public int Value { get; set; }
        public List<GameNode> _gameNodes = new List<GameNode>();
        public GameNode(Game game)
        {
            _game = new Game(game);
        }

        public void FillPossibleMoves(Player playerValue)
        {
            foreach (var moves in _game.GetAllPossiblyMovesForPlayer(playerValue))
            {
                _gameNodes.Add(new GameNode(moves));
            }
        }

        public void AddNode(GameNode gameNode)
        {
            _gameNodes.Add(gameNode);
        }

        //public override string ToString()
        //{
        //    string s = "";

        //    s += Value + " \n";
        //    foreach (var v in _gameNodes)
        //        s += s + v.Value + "\t";



        //    return s;
        //}

    }
}
