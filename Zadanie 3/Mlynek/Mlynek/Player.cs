﻿using System.Diagnostics;
using System.Linq;

namespace Mlynek
{
    public enum GameEndStatus { Winner, Loser, None }
    public enum PlayerOption { Person, AIMiniMax, AIAlfabeta }
    public enum PlayerHeuristics { One, Two, Three, Four}




    public class Player
    {
        public PlayerValue Value { get; set; }
        public int PawnsCount { get; set; } = 0;
        public int PawnsToSet { get; set; } = 9;
        public int PossiblyMoves { get; set; } = -1;
        public GameEndStatus gameEndStatus { get; set; } = GameEndStatus.None;
        public PlayerOption Option { get; set; }

        public short? LastMoveFromRow { get; set; } = null;
        public short? LastMoveFromColumn { get; set; } = null;
        public short? LastMoveToRow { get; set; } = null;
        public short? LastMoveToColumn { get; set; } = null;
        public long TimeCounter { get; set; } = 0;
        public PlayerHeuristics PlayerHeuristic { get; set; }

        public Player(PlayerValue value)
        {
            Value = value;
        }

        public Player(Player player)
        {
            Value = player.Value;
            PawnsCount = player.PawnsCount;
            PawnsToSet = player.PawnsToSet;
            gameEndStatus = player.gameEndStatus;
            Option = player.Option;

            LastMoveFromColumn = player.LastMoveFromColumn;
            LastMoveFromRow = player.LastMoveFromRow;
            LastMoveToColumn = player.LastMoveToColumn;
            LastMoveToRow = player.LastMoveToRow;
            TimeCounter = player.TimeCounter;
            PlayerHeuristic = player.PlayerHeuristic;
        }

        

        public bool HasPossiblyMoves(Board board)
        {
            for (short i = 0; i < Board._boardSize; ++i)
                for (short j = 0; j < Board._boardSize; ++j)
                {
                    var field = board.GetField(i, j);
                    if (field != null)
                        if (field.Value == Value && field.PossiblyMoves.Count > 0)
                            return true;
                }
            return false;
        }

        public Game MakeMove(Game game)
        {
            
            Minimax m = new Minimax(game._CurrentPlayer.Value);
            GameNode gameNode = new GameNode(game);
            int best = 0;
            Stopwatch stopwatch = Stopwatch.StartNew();
            switch (Option)
            {
                case PlayerOption.Person:
                    return null;
                case PlayerOption.AIAlfabeta:
                    best = m.AlfaBeta(gameNode, 3, true, int.MinValue, int.MaxValue, gameNode._game._CurrentPlayer.PlayerHeuristic);
                    stopwatch.Stop();
                    break;
                case PlayerOption.AIMiniMax:
                    best = m.MiniMax(gameNode, 3, true, gameNode._game._CurrentPlayer.PlayerHeuristic);
                    stopwatch.Stop();
                    break;
            }
            var g = new Game(gameNode._gameNodes.Where(x => x.Value == best).First()._game);
            g._CurrentPlayerOponent.TimeCounter+=stopwatch.ElapsedMilliseconds;
            Game.History.Add(g);
            
            return g;

        }

    }
}
