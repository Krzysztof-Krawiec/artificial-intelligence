﻿using System;
using System.Collections.Generic;

namespace Mlynek
{
    public class Field
    {
        public PlayerValue Value { get; set; }
        public short Row { get; set; }
        public short Column { get; set; }
        public List<Tuple<short, short>> PossiblyMoves = new List<Tuple<short, short>>();

        public Field(short row, short column)
        {
            Row = row;
            Column = column;
            Value = PlayerValue.nullValue;
        }

        public Field(Field field)
        {
            Row = field.Row;
            Column = field.Column;
            Value = field.Value;
            foreach(var tuple in field.PossiblyMoves)
            {
                PossiblyMoves.Add(new Tuple<short, short>(tuple.Item1, tuple.Item2));
            }
        }

        public Field(PlayerValue value, short row, short column)
        {
            Value = value;
            Row = row;
            Column = column;
        }

        public void AddPossiblyMoves(short row, short column)
        {
            PossiblyMoves.Add(new Tuple<short, short>(row, column));
        }
    }
}
