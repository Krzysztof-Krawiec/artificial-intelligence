﻿using System;
using System.Linq;

namespace Mlynek
{
    public class Minimax
    {
        private PlayerValue _playerValue;
        public Minimax(PlayerValue playerValue)
        {
            _playerValue = playerValue;
        }

        private void CreatePossiblyMoves(GameNode gameNode)
        {
            gameNode._game.SetCurrentGameState();
            gameNode.FillPossibleMoves(gameNode._game._CurrentPlayer);
        }

        public int MiniMax(GameNode gameNode, int depth, bool maximizingPlayer, PlayerHeuristics heuristic)
        {
            if (depth == 0)
            {
                int tempValue = GetValueForNode(gameNode, heuristic);
                gameNode.Value = tempValue;
                return tempValue;
            }

            CreatePossiblyMoves(gameNode);
            if (gameNode._gameNodes.Count() == 0)
            {
                int tempValue = GetValueForNode(gameNode, heuristic);
                gameNode.Value = tempValue;
                return tempValue;
            }

            if (maximizingPlayer)
            {
                int value = int.MinValue;
                foreach (var node in gameNode._gameNodes)
                {
                    value = Math.Max(value, MiniMax(node, depth - 1, false,heuristic));
                    
                }
                gameNode.Value = value;
                return value;
            }
            else
            {
                int value = int.MaxValue;
                foreach (var node in gameNode._gameNodes)
                {
                    value = Math.Min(value, MiniMax(node, depth - 1, true,heuristic));
                    
                }
                gameNode.Value = value;
                return value;
            }
        }





        public int AlfaBeta(GameNode gameNode, int depth, bool maximizingPlayer, int alfa, int beta,PlayerHeuristics heuristic)
        {
            if (depth == 0)
            {
                int tempValue = GetValueForNode(gameNode,heuristic);
                gameNode.Value = tempValue;
                return tempValue;
            }

            CreatePossiblyMoves(gameNode);
            if (gameNode._gameNodes.Count() == 0)
            {
                int tempValue = GetValueForNode(gameNode, heuristic);
                gameNode.Value = tempValue;
                return tempValue;
            }

            if (maximizingPlayer)
            {
                int value = int.MinValue;
                foreach (var node in gameNode._gameNodes)
                {
                    value = Math.Max(value, AlfaBeta(node, depth - 1, false, alfa, beta,heuristic));
                    alfa = Math.Max(alfa, value);
                    if (alfa >= beta)
                        break;
                }
                gameNode.Value = value;
                return value;

            }
            else
            {
                int value = int.MaxValue;
                foreach (var node in gameNode._gameNodes)
                {
                    value = Math.Min(value, AlfaBeta(node, depth - 1, true, alfa, beta,heuristic));
                    beta = Math.Min(beta, value);
                    if (alfa >= beta)
                        break;
                }
                gameNode.Value = value;
                return value;
            }
        }
        public int GetValueForNode(GameNode gameNode, PlayerHeuristics heuristic)
        {
            switch (heuristic)
            {
                case PlayerHeuristics.One:return GetValueForNode1(gameNode); ;
                case PlayerHeuristics.Two: return GetValueForNode2(gameNode); ;
                case PlayerHeuristics.Three: return GetValueForNode1(gameNode); ;
                case PlayerHeuristics.Four: return GetValueForNode1(gameNode); ;
            }
            return GetValueForNode1(gameNode);
        }

        public int GetValueForNode1(GameNode gameNode)
        {
            Player playerMaximizer;
            Player playerOpponent;
            if (gameNode._game.player1.Value == _playerValue)
            {
                playerMaximizer = gameNode._game.player1;
                playerOpponent = gameNode._game.player2;
            }
            else
            {
                playerMaximizer = gameNode._game.player2;
                playerOpponent = gameNode._game.player1;
            }

            switch (gameNode._game.StateInGame)
            {
                case GameState.beggining:
                    return playerMaximizer.PawnsCount - playerOpponent.PawnsCount;
                case GameState.remis:return 0;
                default:
                    if (playerOpponent.PawnsCount <= 2)
                        return int.MaxValue;
                    if (playerMaximizer.PawnsCount <= 2)
                        return int.MinValue;
                    if (playerOpponent.PossiblyMoves == 0)
                        return int.MaxValue;
                    return (1000 * (playerMaximizer.PawnsCount - playerOpponent.PawnsCount) - playerOpponent.PossiblyMoves);
            }
        }







        public int GetValueForNode2(GameNode gameNode)
        {
            Player playerMaximizer;
            Player playerOpponent;
            if (gameNode._game.player1.Value == _playerValue)
            {
                playerMaximizer = gameNode._game.player1;
                playerOpponent = gameNode._game.player2;
            }
            else
            {
                playerMaximizer = gameNode._game.player2;
                playerOpponent = gameNode._game.player1;
            }
            int maximizerInTheMiddle = gameNode._game.CountValuesInTheMiddle(playerMaximizer.Value);
            int oponentInTheMiddle = gameNode._game.CountValuesInTheMiddle(playerOpponent.Value);
            switch (gameNode._game.StateInGame)
            {
                case GameState.remis: return 0;
                case GameState.beggining:
                    return (1000 * (playerMaximizer.PawnsCount - playerOpponent.PawnsCount) + 10 * (maximizerInTheMiddle - oponentInTheMiddle) - playerOpponent.PossiblyMoves);
                default:
                    if (playerOpponent.PawnsCount <= 2)
                        return int.MaxValue;
                    if (playerMaximizer.PawnsCount <= 2)
                        return int.MinValue;
                    if (playerOpponent.PossiblyMoves == 0)
                        return int.MaxValue;
                    return (1000 * (playerMaximizer.PawnsCount - playerOpponent.PawnsCount) + 10 * (maximizerInTheMiddle - oponentInTheMiddle) - playerOpponent.PossiblyMoves);
            }
        }
    }
}
