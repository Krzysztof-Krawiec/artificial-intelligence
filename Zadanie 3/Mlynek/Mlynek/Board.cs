﻿using System;
using System.Collections.Generic;

namespace Mlynek
{
    public class Board
    {
        public static short _boardSize = 7;
        private Field[,] _board = new Field[_boardSize, _boardSize];

        public Board(Board board)
        {
            for (short i = 0; i < _boardSize; ++i)
                for (short j = 0; j < _boardSize; ++j)
                {
                    if (board._board[i, j] != null)
                        _board[i, j] = new Field(board._board[i, j]);
                }
        }

        public static List<List<Tuple<short, short>>> mills = new List<List<Tuple<short, short>>>()
            {
                new List<Tuple<short , short >>() { new Tuple<short, short>(0, 0), new Tuple<short, short>(0, 3), new Tuple<short, short>(0, 6) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(1, 1), new Tuple<short, short>(1, 3), new Tuple<short, short>(1, 5) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(2, 2), new Tuple<short, short>(2, 3), new Tuple<short, short>(2, 4) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(3, 0), new Tuple<short, short>(3, 1), new Tuple<short, short>(3, 2) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(3, 4), new Tuple<short, short>(3, 5), new Tuple<short, short>(3, 6) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(4, 2), new Tuple<short, short>(4, 3), new Tuple<short, short>(4, 4) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(5, 1), new Tuple<short, short>(5, 3), new Tuple<short, short>(5, 5) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(6, 0), new Tuple<short, short>(6, 3), new Tuple<short, short>(6, 6) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(0, 0), new Tuple<short, short>(3, 0), new Tuple<short, short>(6, 0) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(1, 1), new Tuple<short, short>(3, 1), new Tuple<short, short>(5, 1) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(2, 2), new Tuple<short, short>(3, 2), new Tuple<short, short>(4, 2) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(0, 3), new Tuple<short, short>(1, 3), new Tuple<short, short>(2, 3) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(4, 3), new Tuple<short, short>(5, 3), new Tuple<short, short>(6, 3) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(2, 4), new Tuple<short, short>(3, 4), new Tuple<short, short>(4, 4) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(1, 5), new Tuple<short, short>(3, 5), new Tuple<short, short>(5, 5) },
                new List<Tuple<short, short>>() { new Tuple<short, short>(0, 6), new Tuple<short, short>(3, 6), new Tuple<short, short>(6, 6) }
            };


        public Board()
        {
            _board[0, 0] = new Field(0, 0);
            _board[0, 3] = new Field(0, 3);
            _board[0, 6] = new Field(0, 6);
            _board[1, 1] = new Field(1, 1);
            _board[1, 3] = new Field(1, 3);
            _board[1, 5] = new Field(1, 5);
            _board[2, 2] = new Field(2, 2);
            _board[2, 3] = new Field(2, 3);
            _board[2, 4] = new Field(2, 4);
            _board[3, 0] = new Field(3, 0);
            _board[3, 1] = new Field(3, 1);
            _board[3, 2] = new Field(3, 2);
            _board[3, 4] = new Field(3, 4);
            _board[3, 5] = new Field(3, 5);
            _board[3, 6] = new Field(3, 6);
            _board[4, 2] = new Field(4, 2);
            _board[4, 3] = new Field(4, 3);
            _board[4, 4] = new Field(4, 4);
            _board[5, 1] = new Field(5, 1);
            _board[5, 3] = new Field(5, 3);
            _board[5, 5] = new Field(5, 5);
            _board[6, 0] = new Field(6, 0);
            _board[6, 3] = new Field(6, 3);
            _board[6, 6] = new Field(6, 6);
        }





        public PlayerValue GetValue(short row, short column)
        {
            return _board[row, column].Value;
        }

        public Field GetField(short row, short column)
        {
            return _board[row, column];
        }

        public bool MoveValue(Field field, short row, short column, Player currentPlayer)
        {
            if (currentPlayer.LastMoveFromColumn != null)
            {
                if (currentPlayer.LastMoveFromColumn == column && currentPlayer.LastMoveFromRow == row &&
                    currentPlayer.LastMoveToColumn == field.Column && currentPlayer.LastMoveToRow == field.Row)
                    return false;
            }
            if (_board[row, column].Value == PlayerValue.nullValue && field.Value == currentPlayer.Value &&
                field.PossiblyMoves.Exists(x => x.Item1 == row && x.Item2 == column))
            {
                _board[row, column].Value = currentPlayer.Value;
                field.Value = PlayerValue.nullValue;

                currentPlayer.LastMoveFromColumn = field.Column;
                currentPlayer.LastMoveFromRow = field.Row;
                currentPlayer.LastMoveToRow = row;
                currentPlayer.LastMoveToColumn = column;
                return true;
            }
            else
                return false;
        }

        public bool MoveAnyValue(Field field, short row, short column, Player currentPlayer)
        {
            if (_board[row, column].Value == PlayerValue.nullValue && field.Value == currentPlayer.Value)
            {
                _board[row, column].Value = currentPlayer.Value;
                field.Value = PlayerValue.nullValue;
                return true;
            }
            else
                return false;
        }

        public bool SetValue(short row, short column, Player currentPlayer)
        {
            if (_board[row, column].Value == PlayerValue.nullValue)
            {
                _board[row, column].Value = currentPlayer.Value;
                return true;
            }
            else
                return false;
        }

        public bool IsMill(short row, short column)
        {
            foreach (var mill in mills)
            {
                if (mill.Exists(x => x.Item1 == row && x.Item2 == column))
                    if (_board[mill[0].Item1, mill[0].Item2].Value != PlayerValue.nullValue && _board[mill[1].Item1, mill[1].Item2].Value != PlayerValue.nullValue && _board[mill[2].Item1, mill[2].Item2].Value != PlayerValue.nullValue && _board[mill[0].Item1, mill[0].Item2].Value == _board[mill[1].Item1, mill[1].Item2].Value && _board[mill[1].Item1, mill[1].Item2].Value == _board[mill[2].Item1, mill[2].Item2].Value)
                        return true;
            }
            return false;
        }


        public bool RemoveValueForMill(short row, short column, Player currentPlayer, bool test = false)
        {
            if (_board[row, column] != null && _board[row, column].Value != PlayerValue.nullValue && _board[row, column].Value != currentPlayer.Value)
            {

                if (!IsMill(row, column) || (IsMill(row, column) && OnlyMills(row, column, _board[row, column].Value)))
                {
                    if (!test)
                        _board[row, column].Value = PlayerValue.nullValue;
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }

        private bool OnlyMills(short row, short column, PlayerValue value)
        {
            for (short i = 0; i < _boardSize; ++i)
                for (short j = 0; j < _boardSize; ++j)
                {
                    if (_board[i, j] != null && _board[i, j].Value == value && !IsMill(i, j))
                        return false;
                }
            return true;
        }

        public override string ToString()
        {
            string s = "";
            for (short i = 0; i < _boardSize; ++i)
            {
                for (short j = 0; j < _boardSize; ++j)
                {
                    if (_board[i, j] != null)
                        s += _board[i, j].Value;
                    else
                        s += " ";
                }
                s += "\n";
            }
            return s;
        }
        public int CountValuesInTheMiddle(PlayerValue playerValue)
        {
            int res = 0;
            if (_board[0, 3].Value == playerValue)
                ++res;
            if (_board[1, 3].Value == playerValue)
                ++res;
            if (_board[2, 3].Value == playerValue)
                ++res;
            if (_board[3, 0].Value == playerValue)
                ++res;
            if (_board[3, 1].Value == playerValue)
                ++res;
            if (_board[3, 2].Value == playerValue)
                ++res;
            if (_board[3, 4].Value == playerValue)
                ++res;
            if (_board[3, 5].Value == playerValue)
                ++res;
            if (_board[3, 6].Value == playerValue)
                ++res;
            if (_board[4, 3].Value == playerValue)
                ++res;
            if (_board[5, 3].Value == playerValue)
                ++res;
            if (_board[6, 3].Value == playerValue)
                ++res;
            return res;
        }
    }
}
