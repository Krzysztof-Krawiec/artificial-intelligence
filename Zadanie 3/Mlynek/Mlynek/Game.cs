﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Mlynek
{
    public enum PlayerValue { zero, one, nullValue }
    public enum GameState { beggining, play, ending, mill, winnerPlayer1, winnerPlayer2, remis };

    public class Game
    {
        public static List<Game> History = new List<Game>();

        public int LastMill { get; set; } = 0;

        public bool FieldSelected { get; set; }
        private Field SelectedField { get; set; }

        public Board board;

        public Player player1 = new Player(PlayerValue.zero);
        public Player player2 = new Player(PlayerValue.one);

        public Player _CurrentPlayer { get; set; }
        public Player _CurrentPlayerOponent { get; set; }
        public GameState StateInGame { get; set; }


        public Game()
        {
            board = new Board();
            StateInGame = GameState.beggining;
            _CurrentPlayer = player1;
            _CurrentPlayerOponent = player2;
        }

        public Game(Game game)
        {
            board = new Board(game.board);
            player1 = new Player(game.player1);
            player2 = new Player(game.player2);
            RefreshPossiblyMoves(board, player1, player2);
            StateInGame = game.StateInGame;
            LastMill = game.LastMill;
            if (game._CurrentPlayer.Value == player1.Value)
            {
                _CurrentPlayer = player1;
                _CurrentPlayerOponent = player2;
            }
            else
            {
                _CurrentPlayer = player2;
                _CurrentPlayerOponent = player1;
            }
        }

        public bool MakeMove(short row, short column, bool visual = true)
        {
            bool correctMove = false;
            if (StateInGame != GameState.mill)
                SetCurrentGameState();
            switch (StateInGame)
            {
                case GameState.beggining:
                    if (SetValue(row, column))
                        correctMove = true;
                    break;
                case GameState.play:
                    if (FieldSelected)
                    {
                        if (MoveValue(row, column))
                            correctMove = true;
                        FieldSelected = false;
                    }
                    else
                        SelectField(row, column);
                    break;
                case GameState.mill:
                    if (RemoveValueForMill(row, column))
                    {
                        SetCurrentGameState();
                        correctMove = true;
                        LastMill = 0;
                    }
                    break;
                case GameState.ending:
                    if (FieldSelected)
                    {
                        if (MoveAnyValue(row, column))
                        {
                            correctMove = true;
                        }
                        FieldSelected = false;
                    }
                    else
                        SelectField(row, column);
                    break;
            }

            if (correctMove)
            {
                RefreshPossiblyMoves(board, player1, player2);

                if (board.IsMill(row, column))
                {
                    StateInGame = GameState.mill;
                }
            }
            if (StateInGame != GameState.mill && correctMove && StateInGame < GameState.winnerPlayer1)
            {
                //if(StateInGame!=GameState.beggining)
                    ++LastMill;
                if (visual)
                    History.Add(this);
                if (_CurrentPlayer == player1)
                {
                    _CurrentPlayerOponent = player1;
                    _CurrentPlayer = player2;
                }
                else
                {
                    _CurrentPlayerOponent = player2;
                    _CurrentPlayer = player1;
                }

            }
            return correctMove;
        }

        public void SetCurrentGameState()
        {
            if (_CurrentPlayer.PawnsToSet == 0)
            {
                if (LastMill >= 50)
                {
                    StateInGame = GameState.remis;
                    return;
                }
                if (_CurrentPlayer.PawnsCount>3 && !_CurrentPlayer.HasPossiblyMoves(board))
                {
                    if (_CurrentPlayer.Value==PlayerValue.zero)
                        StateInGame = GameState.winnerPlayer2;
                    else
                        StateInGame = GameState.winnerPlayer1;
                    return;
                }
                
                if (_CurrentPlayer.PawnsCount < 3)
                {
                    if (_CurrentPlayer.Value == PlayerValue.zero)
                        StateInGame = GameState.winnerPlayer2;
                    else
                        StateInGame = GameState.winnerPlayer1;
                    return;
                }
            }
            if (_CurrentPlayer.PawnsToSet > 0)
            {
                StateInGame = GameState.beggining;
                return;
            }
            if (_CurrentPlayer.PawnsCount <= 3)
            {
                StateInGame = GameState.ending;
                return;
            }
            StateInGame = GameState.play;
        }

        private bool MoveAnyValue(short row, short column)
        {
            return board.MoveAnyValue(SelectedField, row, column, _CurrentPlayer);
        }

        public bool SetValue(short row, short column)
        {
            bool result = board.SetValue(row, column, _CurrentPlayer);
            if (result)
            {
                _CurrentPlayer.PawnsToSet -= 1;
                ++_CurrentPlayer.PawnsCount;
            }
            return result;
        }

        private static void AddPossibleFieldsForPlayers(PlayerValue fieldValue, Player player1, Player player2)
        {
            if (fieldValue == PlayerValue.nullValue)
                return;
            if (fieldValue == player1.Value)
                ++player1.PossiblyMoves;
            else
                ++player2.PossiblyMoves;
        }

        private static void AddPossibleFields(Board board, Field field, short row, short column, Player player1, Player player2)
        {
            short columnStep = 0;
            short rowStep = 0;
            switch (row)
            {
                case 0: columnStep = 3; break;
                case 6: columnStep = 3; break;
                case 1: columnStep = 2; break;
                case 5: columnStep = 2; break;
                case 2: columnStep = 1; break;
                case 3: columnStep = 1; break;
                case 4: columnStep = 1; break;
            }
            switch (column)
            {
                case 0: rowStep = 3; break;
                case 6: rowStep = 3; break;
                case 1: rowStep = 2; break;
                case 5: rowStep = 2; break;
                case 2: rowStep = 1; break;
                case 3: rowStep = 1; break;
                case 4: rowStep = 1; break;
            }

            if (row - rowStep >= 0)
            {
                var fieldToAdd = board.GetField((short)(row - rowStep), column);
                if (fieldToAdd != null && fieldToAdd.Value == PlayerValue.nullValue)
                {
                    field.AddPossiblyMoves(fieldToAdd.Row, fieldToAdd.Column);
                    AddPossibleFieldsForPlayers(board.GetValue(row, column), player1, player2);
                }
            }
            if (row + rowStep < Board._boardSize)
            {
                var fieldToAdd = board.GetField((short)(row + rowStep), column);
                if (fieldToAdd != null && fieldToAdd.Value == PlayerValue.nullValue)
                {
                    field.AddPossiblyMoves(fieldToAdd.Row, fieldToAdd.Column);
                    AddPossibleFieldsForPlayers(board.GetValue(row, column), player1, player2);
                }
            }
            if (column - columnStep >= 0)
            {
                var fieldToAdd = board.GetField(row, (short)(column - columnStep));
                if (fieldToAdd != null && fieldToAdd.Value == PlayerValue.nullValue)
                {
                    field.AddPossiblyMoves(fieldToAdd.Row, fieldToAdd.Column);
                    AddPossibleFieldsForPlayers(board.GetValue(row, column), player1, player2);
                }
            }
            if (column + columnStep < Board._boardSize)
            {
                var fieldToAdd = board.GetField(row, (short)(column + columnStep));
                if (fieldToAdd != null && fieldToAdd.Value == PlayerValue.nullValue)
                {
                    field.AddPossiblyMoves(fieldToAdd.Row, fieldToAdd.Column);
                    AddPossibleFieldsForPlayers(board.GetValue(row, column), player1, player2);
                }
            }
        }

        public bool RemoveValueForMill(short row, short column, bool test = false)
        {
            bool result = board.RemoveValueForMill(row, column, _CurrentPlayer, test);
            if (!test && result)
                _CurrentPlayerOponent.PawnsCount -= 1;
            return result;
        }

        public void SelectField(short row, short column)
        {
            if (board.GetField(row, column).Value != PlayerValue.nullValue)
            {
                SelectedField = board.GetField(row, column);
                FieldSelected = true;
            }
        }

        private static void RefreshPossiblyMoves(Board board, Player player1, Player player2)
        {
            player1.PossiblyMoves = 0;
            player2.PossiblyMoves = 0;
            for (short i = 0; i < Board._boardSize; ++i)
                for (short j = 0; j < Board._boardSize; ++j)
                {
                    var field = board.GetField(i, j);
                    if (field != null)
                    {
                        field.PossiblyMoves.Clear();
                        AddPossibleFields(board, field, i, j, player1, player2);
                    }
                }
        }

        public bool MoveValue(short row, short column)
        {
            return board.MoveValue(SelectedField, row, column, _CurrentPlayer);
        }

        public PlayerValue GetValue(short row, short column)
        {
            return board.GetValue(row, column);
        }

        private List<Tuple<short, short>> PossibleMillFieldsToRemove()
        {
            List<Tuple<short, short>> result = new List<Tuple<short, short>>();

            for (short i = 0; i < Board._boardSize; ++i)
                for (short j = 0; j < Board._boardSize; ++j)
                {
                    if (RemoveValueForMill(i, j, true))
                        result.Add(new Tuple<short, short>(i, j));
                }

            return result;
        }

        private bool FillPossibleGameStatesForMill(List<Game> possiblyGamesStates)
        {
            if (StateInGame != GameState.mill)
                return false;

            var millRemoves = PossibleMillFieldsToRemove();
            foreach (var newState in millRemoves)
            {
                var newGame = new Game(this);
                newGame.MakeMove(newState.Item1, newState.Item2, false);
                possiblyGamesStates.Add(newGame);
            }
            return true;
        }

        public List<Game> GetAllPossiblyMovesForPlayer(Player playerValue)
        {
            List<Game> possiblyGameStates = new List<Game>();

            switch (StateInGame)
            {
                case GameState.beggining:
                    for (short i = 0; i < Board._boardSize; ++i)
                        for (short j = 0; j < Board._boardSize; ++j)
                        {
                            var field = board.GetField(i, j);
                            if (field != null && field.Value == PlayerValue.nullValue)
                            {
                                var game = new Game(this);
                                game.MakeMove(i, j, false);
                                if (!game.FillPossibleGameStatesForMill(possiblyGameStates))
                                    possiblyGameStates.Add(game);
                            }
                        }
                    break;
                case GameState.play:
                    for (short i = 0; i < Board._boardSize; ++i)
                        for (short j = 0; j < Board._boardSize; ++j)
                        {

                            var field = board.GetField(i, j);
                            if (field != null && field.Value == playerValue.Value)
                            {
                                foreach (var possiblyMove in field.PossiblyMoves)
                                {
                                    if (playerValue.LastMoveFromColumn != null)
                                    {

                                        if (playerValue.LastMoveFromColumn == possiblyMove.Item2 && playerValue.LastMoveFromRow == possiblyMove.Item1 &&
                                            playerValue.LastMoveToColumn == j && playerValue.LastMoveToRow == i)
                                            break;
                                    }

                                    var game = new Game(this);
                                    game.SelectField(i, j);
                                    game.MakeMove(possiblyMove.Item1, possiblyMove.Item2, false);
                                    if (!game.FillPossibleGameStatesForMill(possiblyGameStates))
                                        possiblyGameStates.Add(game);
                                }
                            }
                        }
                    break;
                case GameState.ending:

                    List<Tuple<short, short>> emptyFields = new List<Tuple<short, short>>();
                    for (short i = 0; i < Board._boardSize; ++i)
                        for (short j = 0; j < Board._boardSize; ++j)
                        {
                            var field = board.GetField(i, j);
                            if (field != null && field.Value == PlayerValue.nullValue)
                                emptyFields.Add(new Tuple<short, short>(i, j));
                        }


                    for (short i = 0; i < Board._boardSize; ++i)
                        for (short j = 0; j < Board._boardSize; ++j)
                        {
                            var field = board.GetField(i, j);
                            if (field != null && field.Value == playerValue.Value)
                            {
                                foreach (var possiblyMove in emptyFields)
                                {
                                    var game = new Game(this);
                                    game.SelectField(i, j);
                                    game.MakeMove(possiblyMove.Item1, possiblyMove.Item2, false);
                                    if (!game.FillPossibleGameStatesForMill(possiblyGameStates))
                                        possiblyGameStates.Add(game);
                                }
                            }
                        }
                    break;
            }
            return possiblyGameStates;
        }

        public int CountValuesInTheMiddle(PlayerValue playerValue)
        {
            return board.CountValuesInTheMiddle(playerValue);

        }
    }
}
