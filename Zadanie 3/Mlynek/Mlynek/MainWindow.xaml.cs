﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Mlynek
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Game g;
        public MainWindow()
        {
            g = new Game();
            g.player2.Option = PlayerOption.AIAlfabeta;
            InitializeComponent();
            Player1.Items.Add("Człowiek");
            Player1.Items.Add("AI Minimax");
            Player1.Items.Add("AI Alfa-beta");

            Player2.Items.Add("Człowiek");
            Player2.Items.Add("AI Minimax");
            Player2.Items.Add("AI Alfa-beta");

            Player1.SelectedIndex = 0;
            Player2.SelectedIndex = 0;
            Player1Heuristic.Items.Add("1");
            Player1Heuristic.Items.Add("2");
            Player1Heuristic.Items.Add("3");
            Player1Heuristic.Items.Add("4");
            Player1Heuristic.SelectedIndex = 0;
            Player2Heuristic.SelectedIndex = 0;
            Player2Heuristic.Items.Add("1");
            Player2Heuristic.Items.Add("2");
            Player2Heuristic.Items.Add("3");
            Player2Heuristic.Items.Add("4");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //0,0
            MakeMove(0, 0);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MakeMove(0, 3);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            //0,2
            MakeMove(0, 6);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            //1,0
            MakeMove(1, 1);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            //1,1  
            MakeMove(1, 3);
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            //1,2
            MakeMove(1, 5);

        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            //2,0
            MakeMove(2, 2);
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            //2,1
            MakeMove(2, 3);
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            //2,2
            MakeMove(2, 4);
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            //3,0
            MakeMove(3, 0);
        }

        private void Button_Click_10(object sender, RoutedEventArgs e)
        {
            //3,1
            MakeMove(3, 1);
        }

        private void Button_Click_11(object sender, RoutedEventArgs e)
        {
            //3,2
            MakeMove(3, 2);
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            //3,3
            MakeMove(3, 4);
        }

        private void Button_Click_13(object sender, RoutedEventArgs e)
        {
            //3,4
            MakeMove(3, 5);
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            //3,5
            MakeMove(3, 6);
        }

        private void Button_Click_15(object sender, RoutedEventArgs e)
        {
            //4,0
            MakeMove(4, 2);
        }

        private void Button_Click_16(object sender, RoutedEventArgs e)
        {
            //4,1
            MakeMove(4, 3);
        }

        private void Button_Click_17(object sender, RoutedEventArgs e)
        {
            //4,2
            MakeMove(4, 4);
        }

        private void Button_Click_18(object sender, RoutedEventArgs e)
        {
            //5,1
            MakeMove(5, 1);
        }

        private void Button_Click_19(object sender, RoutedEventArgs e)
        {
            //5,2
            MakeMove(5, 3);
        }

        private void Button_Click_20(object sender, RoutedEventArgs e)
        {
            //5,3
            MakeMove(5, 5);
        }

        private void Button_Click_21(object sender, RoutedEventArgs e)
        {
            //6,1
            MakeMove(6, 0);
        }

        private void Button_Click_22(object sender, RoutedEventArgs e)
        {
            //6,2
            MakeMove(6, 3);
        }

        private void Button_Click_23(object sender, RoutedEventArgs e)
        {
            //6,3
            MakeMove(6, 6);
        }

        public void RepaintBoard()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                btn0_0.Content = (int)g.GetValue(0, 0) > 1 ? "" : ((int)g.GetValue(0, 0)).ToString();
                btn0_6.Content = (int)g.GetValue(0, 3) > 1 ? "" : ((int)g.GetValue(0, 3)).ToString();
                btn0_12.Content = (int)g.GetValue(0, 6) > 1 ? "" : ((int)g.GetValue(0, 6)).ToString();
                btn2_2.Content = (int)g.GetValue(1, 1) > 1 ? "" : ((int)g.GetValue(1, 1)).ToString();
                btn2_6.Content = (int)g.GetValue(1, 3) > 1 ? "" : ((int)g.GetValue(1, 3)).ToString();
                btn2_10.Content = (int)g.GetValue(1, 5) > 1 ? "" : ((int)g.GetValue(1, 5)).ToString();
                btn4_4.Content = (int)g.GetValue(2, 2) > 1 ? "" : ((int)g.GetValue(2, 2)).ToString();
                btn4_6.Content = (int)g.GetValue(2, 3) > 1 ? "" : ((int)g.GetValue(2, 3)).ToString();
                btn4_8.Content = (int)g.GetValue(2, 4) > 1 ? "" : ((int)g.GetValue(2, 4)).ToString();
                btn6_0.Content = (int)g.GetValue(3, 0) > 1 ? "" : ((int)g.GetValue(3, 0)).ToString();
                btn6_2.Content = (int)g.GetValue(3, 1) > 1 ? "" : ((int)g.GetValue(3, 1)).ToString();
                btn6_4.Content = (int)g.GetValue(3, 2) > 1 ? "" : ((int)g.GetValue(3, 2)).ToString();
                btn6_8.Content = (int)g.GetValue(3, 4) > 1 ? "" : ((int)g.GetValue(3, 4)).ToString();
                btn6_10.Content = (int)g.GetValue(3, 5) > 1 ? "" : ((int)g.GetValue(3, 5)).ToString();
                btn6_12.Content = (int)g.GetValue(3, 6) > 1 ? "" : ((int)g.GetValue(3, 6)).ToString();
                btn8_4.Content = (int)g.GetValue(4, 2) > 1 ? "" : ((int)g.GetValue(4, 2)).ToString();
                btn8_6.Content = (int)g.GetValue(4, 3) > 1 ? "" : ((int)g.GetValue(4, 3)).ToString();
                btn8_8.Content = (int)g.GetValue(4, 4) > 1 ? "" : ((int)g.GetValue(4, 4)).ToString();
                btn10_2.Content = (int)g.GetValue(5, 1) > 1 ? "" : ((int)g.GetValue(5, 1)).ToString();
                btn10_6.Content = (int)g.GetValue(5, 3) > 1 ? "" : ((int)g.GetValue(5, 3)).ToString();
                btn10_10.Content = (int)g.GetValue(5, 5) > 1 ? "" : ((int)g.GetValue(5, 5)).ToString();
                btn12_0.Content = (int)g.GetValue(6, 0) > 1 ? "" : ((int)g.GetValue(6, 0)).ToString();
                btn12_6.Content = (int)g.GetValue(6, 3) > 1 ? "" : ((int)g.GetValue(6, 3)).ToString();
                btn12_12.Content = (int)g.GetValue(6, 6) > 1 ? "" : ((int)g.GetValue(6, 6)).ToString();

                List<int> items = new List<int>();
                for (int i = 1; i <= Game.History.Count; ++i)
                    items.Add(i);
                History.ItemsSource = items;
                LastMill.Content = g.LastMill.ToString();

                Player1Time.Content = g.player1.TimeCounter.ToString();
                Player2Time.Content = g.player2.TimeCounter.ToString();
            }), DispatcherPriority.Background);
            
        }

        private void Play()
        {
            while (g._CurrentPlayer.Option != PlayerOption.Person && g.StateInGame < GameState.winnerPlayer1)
            {
                
                
                g = new Game(g._CurrentPlayer.MakeMove(g));
                RepaintBoard();
            }
        }
        private void MakeMove(short row, short column)
        {
            bool correctMove = false;
            if (g._CurrentPlayer.Option == PlayerOption.Person)
            {
                correctMove = g.MakeMove(row, column);
                if (correctMove)
                    RepaintBoard();
            }
            Task.Factory.StartNew(() => Play());
            switch (g.StateInGame)
            {
                case GameState.remis: MessageBox.Show("REMIS"); RepaintBoard(); break;
                case GameState.winnerPlayer2: MessageBox.Show("Wygrał gracz 2"); RepaintBoard(); break;
                case GameState.winnerPlayer1: MessageBox.Show("Wygrał gracz 1"); RepaintBoard(); break;
            }
                

        }

        private void History_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = (int)History.SelectedItem;
            g = Game.History[selected - 1];
            RepaintBoard();
        }

        private void Player2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((sender as ComboBox).SelectedItem as string)
            {
                case "Człowiek": g.player2.Option = PlayerOption.Person; break;
                case "AI Alfa-beta": g.player2.Option = PlayerOption.AIAlfabeta; break;
                case "AI Minimax": g.player2.Option = PlayerOption.AIMiniMax; break;
            }
        }

        private void Player1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((sender as ComboBox).SelectedItem as string)
            {
                case "Człowiek": g.player1.Option = PlayerOption.Person; break;
                case "AI Alfa-beta": g.player1.Option = PlayerOption.AIAlfabeta; break;
                case "AI Minimax": g.player1.Option = PlayerOption.AIMiniMax; break;
            }
        }

        private void Player2Heuristic_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((sender as ComboBox).SelectedItem as string)
            {
                case "1": g.player2.PlayerHeuristic = PlayerHeuristics.One; break;
                case "2": g.player2.PlayerHeuristic = PlayerHeuristics.Two; break;
                case "3": g.player2.PlayerHeuristic = PlayerHeuristics.Three; break;
                case "4": g.player2.PlayerHeuristic = PlayerHeuristics.Four; break;
            }
        }

        private void Player1Heuristic_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch ((sender as ComboBox).SelectedItem as string)
            {
                case "1": g.player1.PlayerHeuristic = PlayerHeuristics.One; break;
                case "2": g.player1.PlayerHeuristic = PlayerHeuristics.Two; break;
                case "3": g.player1.PlayerHeuristic = PlayerHeuristics.Three; break;
                case "4": g.player1.PlayerHeuristic = PlayerHeuristics.Four; break;
            }
        }
    }
}
