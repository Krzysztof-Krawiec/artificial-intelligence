﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie4
{
    class Program
    {

        public static void Draw(IList<MyPair> pairs)
        {
            Random random = new Random();

            Image img1 = Image.FromFile(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\SI\Zadanie 4\extract_features\first.png");
            Image img2 = Image.FromFile(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\SI\Zadanie 4\extract_features\second.png");

            Pen blackPen = new Pen(Color.FromArgb((byte)random.Next(0, 255), (byte)random.Next(0, 255), (byte)random.Next(0, 255)), 1);

            int outputImageHeight = img1.Height + img2.Height + 1;

            Bitmap outputImage = new Bitmap(img1.Width, outputImageHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (Graphics graphics = Graphics.FromImage(outputImage))
            {
                graphics.DrawImage(img1, new Rectangle(new Point(), img1.Size),
                    new Rectangle(new Point(), img1.Size), GraphicsUnit.Pixel);
                graphics.DrawImage(img2, new Rectangle(new Point(0, img1.Height + 1), img2.Size),
                    new Rectangle(new Point(), img2.Size), GraphicsUnit.Pixel);





                foreach(var pair in pairs)
                {
                    graphics.DrawLine(new Pen(Color.FromArgb((byte)random.Next(0, 255), (byte)random.Next(0, 255), (byte)random.Next(0, 255)), 1), 
                        pair.First.X, pair.First.Y, pair.Second.X, pair.Second.Y+img1.Height);
                }



                
                    
            }
            outputImage.Save(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\SI\Zadanie 4\extract_features\wynik.png", System.Drawing.Imaging.ImageFormat.Jpeg);
        }

        static void Main(string[] args)
        {
            Feature f = new Feature(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\SI\Zadanie 4\extract_features\first.png.haraff.sift");
            Feature ff = new Feature(@"C:\Users\Krzysiek\Desktop\Semestr 6\SI\SI\Zadanie 4\extract_features\second.png.haraff.sift");

            var pairs = f.GetPairs(ff);
            var ransac = Feature.Ransac(pairs, Transformation.perspektywiczna);
            Draw(ransac);
            
            var a = 5;
        }
    }
}
