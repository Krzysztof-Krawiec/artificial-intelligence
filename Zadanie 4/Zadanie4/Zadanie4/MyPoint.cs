﻿using System;
using System.Collections.Generic;

namespace Zadanie4
{
    public class MyPoint
    {
        public float X { get; set; }
        public float Y { get; set; }
        public IList<int> Features { get; set; }

        public MyPoint(float x, float y)
        {
            Features = new List<int>();
            X = x;
            Y = y;
        }

        public double GetDistance(MyPoint secondPoint)
        {
            int res = 0;
            for(int i=0;i<Features.Count;++i)
            {
                res += (Features[i] - secondPoint.Features[i]) * (Features[i] - secondPoint.Features[i]);
            }
            return Math.Sqrt(res);
        }

        public MyPoint GetNearestPoint(Feature feature)
        {
            double min = double.MaxValue;
            MyPoint best = null;
            for(int i=0;i<feature.NumberOfPoints;++i)
            {
                double distance = GetDistance(feature.Points[i]);
                if(distance<min)
                {
                    best = feature.Points[i];
                    min = distance;
                }
            }
            return best;
        }
        

    }
}