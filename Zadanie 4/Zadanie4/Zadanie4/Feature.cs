﻿using MathNet.Numerics.LinearAlgebra;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Zadanie4
{
    public enum Transformation { afiniczna, perspektywiczna };
    public class Feature
    {

        public int FeaturesCounter { get; set; }
        public int NumberOfPoints { get; set; }
        public IList<MyPoint> Points { get; set; }

        public Feature(string filePath)
        {
            using (StreamReader file = new StreamReader(filePath))
            {
                FeaturesCounter = int.Parse(file.ReadLine());
                NumberOfPoints = int.Parse(file.ReadLine());
                Points = new List<MyPoint>();

                for (int i = 0; i < NumberOfPoints; ++i)
                {
                    var line = file.ReadLine();
                    var elements = line.Split(' ');

                    MyPoint p = new MyPoint(float.Parse(elements[0], System.Globalization.CultureInfo.InvariantCulture), float.Parse(elements[1], System.Globalization.CultureInfo.InvariantCulture));
                    for (int j = 5; j < FeaturesCounter; ++j)
                    {
                        p.Features.Add(int.Parse(elements[j]));
                    }
                    Points.Add(p);
                }
            }
        }

        public IList<MyPair> GetPairs(Feature f)
        {
            IList<MyPair> res = new List<MyPair>();
            for (int i = 0; i < NumberOfPoints; ++i)
            {
                MyPoint best = Points[i].GetNearestPoint(f);
                if (best.GetNearestPoint(this) == Points[i])
                {
                    res.Add(new MyPair(Points[i], best));
                }
            }
            return res;
        }


        public static double CountDistance(MyPoint p1, MyPoint p2)
        {
            return Math.Sqrt(((p1.X - p2.X) * (p1.X - p2.X)) + ((p1.Y - p2.Y) * (p1.Y - p2.Y)));
        }

        public IList<MyPair> FilterCohesion(IList<MyPair> pairs)
        {
            int iloscSasiadow = 4;
            double minimum = 0.5;
            IList<MyPair> res = new List<MyPair>();
            foreach (var pair in pairs)
            {
                var a = pairs.Select(x => x.First).Where(x => x != pair.First).OrderBy(x => CountDistance(x, pair.First)).Take(iloscSasiadow).ToList();
                var b = pairs.Select(x => x.Second).Where(x => x != pair.Second).OrderBy(x => CountDistance(x, pair.Second)).Take(iloscSasiadow).ToList();

                int counter = pairs.Count(x => x != pair && a.Contains(x.First) && b.Contains(x.Second));
                if (counter * 1.0 / iloscSasiadow >= minimum)
                {
                    res.Add(pair);
                }
            }
            return res;
        }

        public static IList<MyPair> Ransac(IList<MyPair> pairs, Transformation transform)
        {
            int ransacIterations = 1000;
            double ransacMaxError = 0.05 * Math.Sqrt(1020 * 1020 + 510* 510);
            IList<MyPair> result = new List<MyPair>();
            double bestScore = double.MinValue;

            for (int i = 0; i < ransacIterations; ++i)
            {
                Matrix<double> model = null;
                while (model == null)
                {
                    if (transform == Transformation.afiniczna)
                    {
                        model = AffineTransformation(GetRandomPairs(3, pairs));
                    }
                    else
                    {
                        model = PerspectiveTransformation(GetRandomPairs(4, pairs));
                    }
                }
                IList<MyPair> maybePairs = new List<MyPair>();
                foreach (var pair in pairs)
                {
                    double error = ModelError(model, pair);

                    if (error < ransacMaxError)
                    {
                        maybePairs.Add(pair);
                    }

                    if (maybePairs.Count > bestScore)
                    {
                        bestScore = maybePairs.Count;
                        result.Clear();
                        foreach (var p in maybePairs)
                        {
                            result.Add(p);
                        }
                    }
                }
            }
            return result;
        }

        private static double ModelError(Matrix<double> model, MyPair pair)
        {

            var second = Matrix<double>.Build.Dense(3,1);
            second[0,0] = pair.First.X;
            second[1,0] = pair.First.Y;
            second[2,0] = 1;

            var matrix = model.Multiply(second);

            return Math.Sqrt(
                    ((matrix[0,0] - pair.Second.X) * (matrix[0,0] - pair.Second.X))
                    +
                    ((matrix[1,0] - pair.Second.Y) * (matrix[1,0] - pair.Second.Y))
            );
        }


        private static Matrix<double> AffineTransformation(IList<MyPair> pairs)
        {
            if (pairs.Count() != 3)
            {
                throw new Exception();
            }

            var matrix1 = Matrix<double>.Build.Dense(6, 6);

            matrix1[0,0] = pairs[0].First.X;
            matrix1[1,0] = pairs[1].First.X;
            matrix1[2,0] = pairs[2].First.X;
            matrix1[3,0] = 0;
            matrix1[4,0] = 0;
            matrix1[5,0] = 0;
                     
            matrix1[0,1] = pairs[0].First.Y;
            matrix1[1,1] = pairs[1].First.Y;
            matrix1[2,1] = pairs[2].First.Y;
            matrix1[3,1] = 0;
            matrix1[4,1] = 0;
            matrix1[5,1] = 0;
                     
            matrix1[0,2] = 1;
            matrix1[1,2] = 1;
            matrix1[2,2] = 1;
            matrix1[3,2] = 0;
            matrix1[4,2] = 0;
            matrix1[5,2] = 0;
                     
            matrix1[0,3] = 0;
            matrix1[1,3] = 0;
            matrix1[2,3] = 0;
            matrix1[3,3] = pairs[0].First.X;
            matrix1[4,3] = pairs[1].First.X;
            matrix1[5,3] = pairs[2].First.X;
                     
            matrix1[0,4] = 0;
            matrix1[1,4] = 0;
            matrix1[2,4] = 0;
            matrix1[3,4] = pairs[0].First.Y;
            matrix1[4,4] = pairs[1].First.Y;
            matrix1[5,4] = pairs[2].First.Y;
                     
            matrix1[0,5] = 0;
            matrix1[1,5] = 0;
            matrix1[2,5] = 0;
            matrix1[3,5] = 1;
            matrix1[4,5] = 1;
            matrix1[5,5] = 1;

            var matrix2 = Matrix<double>.Build.Dense(6, 1);
            matrix2[0,0] = pairs[0].Second.X;
            matrix2[1,0] = pairs[1].Second.X;
            matrix2[2,0] = pairs[2].Second.X;
            matrix2[3,0] = pairs[0].Second.Y;
            matrix2[4,0] = pairs[1].Second.Y;
            matrix2[5,0] = pairs[2].Second.Y;

            var inverted = matrix1.Inverse();
            var multiplied = inverted.Multiply(matrix2);

            var result = Matrix<double>.Build.Dense(3, 3);

            result[0,0] = multiplied[0,0];
            result[1,0] = multiplied[3,0];
            result[2,0] = 0;

            result[0,1] = multiplied[1,0];
            result[1,1] = multiplied[4,0];
            result[2,1] = 0;

            result[0,2] = multiplied[2,0];
            result[1,2] = multiplied[5,0];
            result[2,2] = 1;
            return result;
        }

        private static Matrix<double> PerspectiveTransformation(IList<MyPair> pairs)
        {
            if (pairs.Count != 4)
            {
                throw new Exception();
            }

            var matrix1 = Matrix<double>.Build.Dense(8, 8);

            matrix1[0,0] = pairs[0].First.X;
            matrix1[1,0] = pairs[1].First.X;
            matrix1[2,0] = pairs[2].First.X;
            matrix1[3,0] = pairs[3].First.X;
            matrix1[4,0] = 0;
            matrix1[5,0] = 0;
            matrix1[6,0] = 0;
            matrix1[7,0] = 0;
                     
            matrix1[0,1] = pairs[0].First.Y;
            matrix1[1,1] = pairs[1].First.Y;
            matrix1[2,1] = pairs[2].First.Y;
            matrix1[3,1] = pairs[3].First.Y;
            matrix1[4,1] = 0;
            matrix1[5,1] = 0;
            matrix1[6,1] = 0;
            matrix1[7,1] = 0;
                     
            matrix1[0,2] = 1;
            matrix1[1,2] = 1;
            matrix1[2,2] = 1;
            matrix1[3,2] = 1;
            matrix1[4,2] = 0;
            matrix1[5,2] = 0;
            matrix1[6,2] = 0;
            matrix1[7,2] = 0;
                     
            matrix1[0,3] = 0;
            matrix1[1,3] = 0;
            matrix1[2,3] = 0;
            matrix1[3,3] = 0;
            matrix1[4,3] = pairs[0].First.X;
            matrix1[5,3] = pairs[1].First.X;
            matrix1[6,3] = pairs[2].First.X;
            matrix1[7,3] = pairs[3].First.X;
                     
            matrix1[0,4] = 0;
            matrix1[1,4] = 0;
            matrix1[2,4] = 0;
            matrix1[3,4] = 0;
            matrix1[4,4] = pairs[0].First.Y;
            matrix1[5,4] = pairs[1].First.Y;
            matrix1[6,4] = pairs[2].First.Y;
            matrix1[7,4] = pairs[3].First.Y;
                     
            matrix1[0,5] = 0;
            matrix1[1,5] = 0;
            matrix1[2,5] = 0;
            matrix1[3,5] = 0;
            matrix1[4,5] = 1;
            matrix1[5,5] = 1;
            matrix1[6,5] = 1;
            matrix1[7,5] = 1;
                     
            matrix1[0,6] = -1 * pairs[0].Second.X * pairs[0].First.X;
            matrix1[1,6] = -1 * pairs[1].Second.X * pairs[1].First.X;
            matrix1[2,6] = -1 * pairs[2].Second.X * pairs[2].First.X;
            matrix1[3,6] = -1 * pairs[3].Second.X * pairs[3].First.X;
            matrix1[4,6] = -1 * pairs[0].Second.Y * pairs[0].First.X;
            matrix1[5,6] = -1 * pairs[1].Second.Y * pairs[1].First.X;
            matrix1[6,6] = -1 * pairs[2].Second.Y * pairs[2].First.X;
            matrix1[7,6] = -1 * pairs[3].Second.Y * pairs[3].First.X;
                     
            matrix1[0,7] = -1 * pairs[0].Second.X * pairs[0].First.Y;
            matrix1[1,7] = -1 * pairs[1].Second.X * pairs[1].First.Y;
            matrix1[2,7] = -1 * pairs[2].Second.X * pairs[2].First.Y;
            matrix1[3,7] = -1 * pairs[3].Second.X * pairs[3].First.Y;
            matrix1[4,7] = -1 * pairs[0].Second.Y * pairs[0].First.Y;
            matrix1[5,7] = -1 * pairs[1].Second.Y * pairs[1].First.Y;
            matrix1[6,7] = -1 * pairs[2].Second.Y * pairs[2].First.Y;
            matrix1[7,7] = -1 * pairs[3].Second.Y * pairs[3].First.Y;

            var matrix2 = Matrix<double>.Build.Dense(8, 1);

            matrix2[0,0] = pairs[0].Second.X;
            matrix2[1,0] = pairs[1].Second.X;
            matrix2[2,0] = pairs[2].Second.X;
            matrix2[3,0] = pairs[3].Second.Y;
            matrix2[4,0] = pairs[0].Second.Y;
            matrix2[5,0] = pairs[1].Second.Y;
            matrix2[6,0] = pairs[2].Second.Y;
            matrix2[7,0] = pairs[3].Second.Y;

            var inverted = matrix1.Inverse();
            var multiplied = inverted.Multiply(matrix2);

            var result = Matrix<double>.Build.Dense(3, 3);

            result[0,0] = multiplied[0,0];
            result[1,0] = multiplied[3,0];
            result[2,0] = multiplied[6,0];
                                     
            result[0,1] = multiplied[1,0];
            result[1,1] = multiplied[4,0];
            result[2,1] = multiplied[7,0];
                                     
            result[0,2] = multiplied[2,0];
            result[1,2] = multiplied[5,0];
            result[2,2] = 1;
            return result;

        }

        private static IList<MyPair> GetRandomPairs(int v, IList<MyPair> pairs)
        {
            Random r = new Random();

            IList<int> numbers = new List<int>();
            while (numbers.Distinct().Count() < v)
            {
                numbers.Add(r.Next(0, pairs.Count()));
            }

            var res = new List<MyPair>();
            foreach (var i in numbers.Distinct())
            {
                res.Add(pairs[i]);
            }
            return res;

        }
    }
}
