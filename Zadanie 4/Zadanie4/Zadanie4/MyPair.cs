﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie4
{
    public class MyPair
    {
        public MyPoint First;
        public MyPoint Second;

        public MyPair(MyPoint first, MyPoint second)
        {
            First = first;
            Second = second;
        }
    }
}
